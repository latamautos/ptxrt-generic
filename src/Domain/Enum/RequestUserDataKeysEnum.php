<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Domain\Enum;

use MabeEnum\Enum;

class RequestUserDataKeysEnum extends Enum {
	const USER_DATA = "USER_DATA_USER";
}