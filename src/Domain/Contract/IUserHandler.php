<?php
namespace Latamautos\Ptxrt\Generic\Domain\Contract;

interface IUserHandler {

    public function signUp($email, $password, $name);

    public function recoverPassword($email);

}