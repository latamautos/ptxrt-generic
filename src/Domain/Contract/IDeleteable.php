<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Domain\Contract;


interface IDeleteable {

	public function delete($model);
}