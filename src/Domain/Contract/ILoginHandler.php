<?php
namespace Latamautos\Ptxrt\Generic\Domain\Contract;

interface ILoginHandler {

    public function isUserLoggedIn();

    public function loginViaOldPTX($user, $password);

    public function loginFacebookViaOldPTX($facebookId, $email, $firstName, $lastName);

}