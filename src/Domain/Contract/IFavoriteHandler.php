<?php
namespace Latamautos\Ptxrt\Generic\Domain\Contract;

interface IFavoriteHandler
{

    public function isVehicleFavorited($vehicleId);

    public function favoriteVehicle($vehicleId);

    public function removeFavoriteVehicle($vehicleId);

    public function setUserFavoritesInSession();

    public function favoriteSessionVehicles();

}