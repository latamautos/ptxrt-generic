<?php
namespace Latamautos\Ptxrt\Generic\Domain\Impl;

use Latamautos\Ptxrt\Generic\Domain\Contract\IFavoriteHandler;
use Latamautos\Ptxrt\Generic\Domain\Contract\ILoginHandler;
use Latamautos\Ptxrt\Generic\Infrastructure\Contract\IFavoriteRepository;
use Latamautos\Ptxrt\Generic\Infrastructure\Impl\FavoriteRepository;
use Latamautos\Ptxrt\Utils\Enum\SessionKeysEnum;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;

/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 8/31/15
 * Time: 1:51 PM
 */
class FavoriteHandler implements IFavoriteHandler
{

    private $sessionImpl;
    private $favoriteRepository;
    private $loginHandler;

    private $vehicleIdArray = array();

    function __construct(SessionImpl $sessionImpl, IFavoriteRepository $favoriteRepository, ILoginHandler $loginHandler)
    {
        $this->sessionImpl = $sessionImpl;
        $this->favoriteRepository = $favoriteRepository;
        $this->loginHandler = $loginHandler;
        $favoriteIdsArray = $this->sessionImpl->get(SessionKeysEnum::FAVORITE_VEHICLES_KEY);
        if (isset($favoriteIdsArray)) {
            $this->vehicleIdArray = $favoriteIdsArray;
        }
    }

    public function isVehicleFavorited($vehicleId)
    {
        return in_array($vehicleId, $this->vehicleIdArray);
    }

    public function favoriteVehicle($vehicleId)
    {
        if (!$this->isVehicleFavorited($vehicleId)) {
            array_push($this->vehicleIdArray, $vehicleId + 0);
        }
        $this->sessionImpl->set(SessionKeysEnum::FAVORITE_VEHICLES_KEY, $this->vehicleIdArray);
        if ($this->loginHandler->isUserLoggedIn()) {
            $this->favoriteRepository->callRemoteAddFavorite($vehicleId);
        }
    }

    public function removeFavoriteVehicle($vehicleId)
    {
        if ($this->isVehicleFavorited($vehicleId)) {
            $this->vehicleIdArray = $this->unsetValue($this->vehicleIdArray, $vehicleId + 0);
        }
        $this->sessionImpl->set(SessionKeysEnum::FAVORITE_VEHICLES_KEY, $this->vehicleIdArray);
        if ($this->loginHandler->isUserLoggedIn()) {
            $this->favoriteRepository->callRemoteRemoveFavorite($vehicleId);
        }
    }

    public function setUserFavoritesInSession()
    {
        if ($this->loginHandler->isUserLoggedIn()) {
            $sessionVehicleIds = $this->sessionImpl->get(SessionKeysEnum::FAVORITE_VEHICLES_KEY);
            $vehicleIds = $this->favoriteRepository->callRemoteListFavoritesForLoggedInUser();
            if(is_array($vehicleIds) && is_array($sessionVehicleIds)){
                $vehicleIds = $array = array_merge($vehicleIds, $sessionVehicleIds);
            }
            $this->sessionImpl->set(SessionKeysEnum::FAVORITE_VEHICLES_KEY, $vehicleIds);

        }
    }

    public function favoriteSessionVehicles()
    {
        $vehicleIds = $this->sessionImpl->get(SessionKeysEnum::FAVORITE_VEHICLES_KEY);
        if (is_array($vehicleIds) && isset($vehicleIds)) {
            foreach ($vehicleIds as $vehicleId) {
                $this->favoriteVehicle($vehicleId);
            }
        }
    }

    private function unsetValue(array $array, $value, $strict = TRUE)
    {
        if (($key = array_search($value, $array, $strict)) !== FALSE) {
            unset($array[$key]);
        }
        return $array;
    }

}