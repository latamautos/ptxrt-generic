<?php
namespace Latamautos\Ptxrt\Generic\Domain\Impl;

use Latamautos\Ptxrt\Generic\Domain\Contract\ILoginHandler;
use Latamautos\Ptxrt\Generic\Infrastructure\Contract\ILoginRepository;
use Latamautos\Ptxrt\Utils\Enum\Message500CodeEnum;
use Latamautos\Ptxrt\Utils\Enum\SessionKeysEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;

/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 8/31/15
 * Time: 1:51 PM
 */
class LoginHandler extends BaseService implements ILoginHandler
{

    private $sessionImpl;
    private $loginRepository;

    function __construct(SessionImpl $sessionImpl, ILoginRepository $loginRepository)
    {
        $this->sessionImpl = $sessionImpl;
        $this->loginRepository = $loginRepository;
    }

    public function isUserLoggedIn()
    {
        $userId = $this->sessionImpl->get(SessionKeysEnum::USER_ID_KEY);
        return isset($userId);
    }

    public function loginViaOldPTX($user, $password)
    {
        $response = $this->loginRepository->callRemoteLogin($user, $password);
        $result = json_decode($response);
        if ($result->status === 200) {
            $this->sessionImpl->set(SessionKeysEnum::USER_ID_KEY, $result->data->user_id);
            $this->sessionImpl->set(SessionKeysEnum::USER_TOKEN_KEY, $result->data->token);
        } else {
            $this->getNotifications()->addError(Message500CodeEnum::USER_PASSWORD_INCORRECT());
        }
    }

    public function loginFacebookViaOldPTX($facebookId, $email, $firstName, $lastName)
    {
        $response = $this->loginRepository->callRemoteLoginFacebook($facebookId, $email, $firstName, $lastName);
        $result = json_decode($response);
        if ($result->status === 200) {
            $this->sessionImpl->set(SessionKeysEnum::USER_ID_KEY, $result->data->user_id);
            $this->sessionImpl->set(SessionKeysEnum::USER_TOKEN_KEY, $result->data->token);
        } else {
            $this->getNotifications()->addError(Message500CodeEnum::USER_PASSWORD_INCORRECT());
        }
    }
}