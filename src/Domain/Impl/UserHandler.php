<?php
namespace Latamautos\Ptxrt\Generic\Domain\Impl;

use Latamautos\Ptxrt\Generic\Domain\Contract\IUserHandler;
use Latamautos\Ptxrt\Generic\Infrastructure\Contract\IUserRepository;
use Latamautos\Ptxrt\Utils\Enum\Message500CodeEnum;
use Latamautos\Ptxrt\Utils\Enum\SessionKeysEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;

class UserHandler extends BaseService implements IUserHandler
{

    private $sessionImpl;
    private $userRepository;

    function __construct(SessionImpl $sessionImpl, IUserRepository $userRepository)
    {
        $this->sessionImpl = $sessionImpl;
        $this->userRepository = $userRepository;
    }

    public function signUp($email, $password, $name)
    {
        $response = $this->userRepository->callRemoteSignUp($email, $password, $name);
        $result = json_decode($response);
        if ($result->status === 200) {
            $this->sessionImpl->set(SessionKeysEnum::USER_ID_KEY, $result->data->user_id);
            $this->sessionImpl->set(SessionKeysEnum::USER_TOKEN_KEY, $result->data->token);
        } else {
            $this->getNotifications()->addError(Message500CodeEnum::USER_NOT_CREATED());
        }
    }

    public function recoverPassword($email)
    {
        $this->userRepository->callRemoteRecoverPassword($email);
    }
}