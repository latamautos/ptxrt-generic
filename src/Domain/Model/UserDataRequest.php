<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 12/4/15
 * Time: 12:36 PM
 */

namespace Latamautos\Ptxrt\Generic\Domain\Model;

class UserDataRequest {

    private $ip;
    private $fullUrl;
    private $userAgentPlatform;
    private $userAgentBrowser;
    private $userAgentVersion;

		const HOST 					= 'host';
		const QUERY_STRING 	= "QUERY_STRING";
		const AMPERSAND 		= '&';
		const EQUALS 				= "=";
		const SITE	 				= 'site';

	function __construct($ip, $fullUrl, $userAgentPlatform, $userAgentBrowser, $userAgentVersion) {
        $this->ip = $ip;
        $this->fullUrl = $fullUrl;
        $this->userAgentPlatform = $userAgentPlatform;
        $this->userAgentBrowser = $userAgentBrowser;
        $this->userAgentVersion = $userAgentVersion;
    }

    public function getIp() {
        return $this->ip;
    }

    public function getFullUrl() {
        return $this->fullUrl;
    }

    public function getUserAgentPlatform() {
        return $this->userAgentPlatform;
    }

    public function getUserAgentBrowser() {
        return $this->userAgentBrowser;
    }

    public function getUserAgentVersion() {
        return $this->userAgentVersion;
    }

		/**
	 * @author Livan Frometa <lfrometa@latamautos.com>
	 */
		public function getDomain() {
			$parse = parse_url($this->fullUrl);
			return $parse[self::HOST];
		}

		/**
	 * @author Livan Frometa <lfrometa@latamautos.com>
	 */
		public function getSiteByQueryString(){
			$queryString = isset($_SERVER[self::QUERY_STRING])?$_SERVER[self::QUERY_STRING]:'';
			if (empty($queryString)) return;
			$keyValueStringArray = explode(self::AMPERSAND, $queryString);
			foreach ($keyValueStringArray as $keyValuePairString) {
				$keyValuePairArray = explode(self::EQUALS, $keyValuePairString);
				if(count($keyValuePairArray)>1 && $keyValuePairArray[0] == self::SITE){
					return $keyValuePairArray[1];
				}
			}
			return;
		}

}