<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 1/7/16
 * Time: 1:53 PM
 */

namespace Latamautos\Ptxrt\Generic\Application\Impl;

use Latamautos\Ptxrt\Generic\Application\Contract\IUserService;
use Latamautos\Ptxrt\Generic\Domain\Contract\IFavoriteHandler;
use Latamautos\Ptxrt\Generic\Domain\Contract\ILoginHandler;
use Latamautos\Ptxrt\Generic\Domain\Contract\IUserHandler;

class UserService implements IUserService
{

    private $loginHandler;
    private $favoriteHandler;
    private $userHandler;

    function __construct(ILoginHandler $loginHandler, IFavoriteHandler $favoriteHandler, IUserHandler $userHandler)
    {
        $this->loginHandler = $loginHandler;
        $this->favoriteHandler = $favoriteHandler;
        $this->userHandler = $userHandler;
    }

    public function signUp($email, $password, $name)
    {
        $this->userHandler->signUp($email, $password, $name);
        if ($this->loginHandler->isUserLoggedIn()) {
            $this->favoriteHandler->favoriteSessionVehicles();
            $this->favoriteHandler->setUserFavoritesInSession();
        }
    }

    public function recoverPassword($email)
    {
        $this->userHandler->recoverPassword($email);
    }
}