<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 1/7/16
 * Time: 1:53 PM
 */

namespace Latamautos\Ptxrt\Generic\Application\Impl;

use Latamautos\Ptxrt\Generic\Application\Contract\IFavoriteService;
use Latamautos\Ptxrt\Generic\Domain\Contract\IFavoriteHandler;

class FavoriteService implements IFavoriteService
{

    private $favoriteHandler;

    function __construct(IFavoriteHandler $favoriteHandler)
    {
        $this->favoriteHandler = $favoriteHandler;
    }


    public function favoriteVehicle($vehicleId)
    {
        $this->favoriteHandler->favoriteVehicle($vehicleId);
    }

    public function removeFavoriteVehicle($vehicleId)
    {
        $this->favoriteHandler->removeFavoriteVehicle($vehicleId);
    }
}