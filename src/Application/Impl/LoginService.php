<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 1/7/16
 * Time: 1:53 PM
 */

namespace Latamautos\Ptxrt\Generic\Application\Impl;

use Latamautos\Ptxrt\Generic\Application\Contract\ILoginService;
use Latamautos\Ptxrt\Generic\Domain\Contract\IFavoriteHandler;
use Latamautos\Ptxrt\Generic\Domain\Contract\ILoginHandler;

class LoginService implements ILoginService
{

    private $loginHandler;
    private $favoriteHandler;

    function __construct(ILoginHandler $loginHandler, IFavoriteHandler $favoriteHandler)
    {
        $this->loginHandler = $loginHandler;
        $this->favoriteHandler = $favoriteHandler;
    }

    public function LoginAndProcessFavorites($user, $password)
    {
        $this->loginHandler->loginViaOldPTX($user, $password);
        $this->processFavorites();
    }

    public function LoginFacebookAndProcessFavorites($facebookId, $email, $firstName, $lastName)
    {
        $this->loginHandler->loginFacebookViaOldPTX($facebookId, $email, $firstName, $lastName);
        $this->processFavorites();
    }

    private function processFavorites(){
        if ($this->loginHandler->isUserLoggedIn()) {
            $this->favoriteHandler->favoriteSessionVehicles();
            $this->favoriteHandler->setUserFavoritesInSession();
        }
    }



}