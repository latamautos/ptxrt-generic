<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Application\Contract;

interface IUserService {

	public function signUp($email, $password, $name);

    public function recoverPassword($email);

}