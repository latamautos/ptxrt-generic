<?php
namespace Latamautos\Ptxrt\Generic\Application\Contract;

interface IFavoriteService
{

    public function favoriteVehicle($vehicleId);

    public function removeFavoriteVehicle($vehicleId);

}