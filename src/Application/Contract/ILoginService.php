<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Application\Contract;

interface ILoginService {

    public function LoginFacebookAndProcessFavorites($facebookId, $email, $firstName, $lastName);

	public function LoginAndProcessFavorites($user, $password);

}