<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;

class UtilEnum extends Enum {

	const PARENT 	= 'parent';
	const VALUE 	= 'value';
	const LABELS 	= 'labels.';
	const TODOS 	= 'todos';
	const FULL_TEXT_SEARCH	= 'fts';

	const TYPE_PREFIX 		= 'TYPE_PREFIX';
	const SUB_TYPE_PREFIX = 'SUB_TYPE_PREFIX';
	const BRAND_PREFIX 		= 'BRAND_PREFIX';
	const MODEL_PREFIX 		= 'MODEL_PREFIX';
	const PROVINCE_PREFIX = 'PROVINCE_PREFIX';
	const CITY_PREFIX 		= 'CITY_PREFIX';
	const COLOR_PREFIX 		= 'COLOR_PREFIX';
	const SECTION_PREFIX 	= 'SECTION_PREFIX';
	const NEGOTIABLE_PREFIX 	= 'NEGOTIABLE_PREFIX';

	const SECTION_NEW 		= 'NEW';
	const SECTION_USED 		= 'USED';
	const SECTION_RENTAL 	= 'RENTAL';

	const NEGOTIABLE_FIXED 				= 'FIXED';
	const NEGOTIABLE_NEGOTIABLE 	= 'NEGOTIABLE';
	const NEGOTIABLE_SAVING_PLAN 	= 'SAVING_PLAN';
	const NEGOTIABLE_FINANCED 		= 'FINANCED';
}