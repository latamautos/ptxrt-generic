<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;

class CatalogPrefixEnum extends Enum {

	const TYPE 						= "type";
	const SUBTYPE 				= "subtype";
	const BRAND 					= "brand";
	const MODEL 					= "model";
	const PROVINCE 				= 'province';
	const CITY 						= 'city';
	const PRICE 					= 'price';
	const MILEAGE 				= 'mileage-km';
	const DRIVE 					= 'drive';
	const FUEL 						= 'fuel';						//Combustible
	const STEERING 				= 'steering';				//Dirección
	const INTERIOR 				= 'interior';				//Tapizado
	const WINDOW 					= 'window';
	const COLOR 					= 'color';					//Color
	const LAST_DIGIT 			= 'last-digit';
	const TRANSMISSION 		= 'transmission';
	const HEATING_SYSTEM 	= 'heating-system'; //Aire
	const FRAME 					= 'frame';
	const SUSPENSION 			= 'suspension';
	const BRAKE 					= 'brake';
	const GEAR 						= 'gear';
	const RIM 						= 'rim';
	const YEAR 						= 'year';
	const MOTOR 					= 'motor';
	const SECTION 				= 'section';
	const CC 							= 'cc';							//Cilindraje
	const EXTRA 					= 'extra';					//Extras
	const SPECIAL_TYPE 		= 'special-type';		//Tipo Especial
	const SPECIAL_EXTRA		= 'special-extra';	//Tipo extra
	const DEALER 					= 'dealer';					//Dealer
	const PLAN 						= 'plan';						//Plan
	const DEALER_TAG 			= 'dealer-tag';			//Dealer Tag
	const ENGINE 					= 'engine';
	const NEGOTIABLE 			= 'negotiable';

	const AUTOS 					= 'autos';
	const MOTOS 					= 'motos';
	const CAMIONES 				= 'camiones';
	const NAUTICA 				= 'nautica';
	const MAQUINARIAS 		= 'maquinarias';
	const BICICLETAS 			= 'bicicletas';
	const OTROS 					= 'otros';
	const AEREO 					= 'aereo';

	const RELEVANCE			  = "relevance";
	const PUBLISHED_AT	  = "published-at";
	const PUBLISHED	  		= "published";
	const CONNECTED	  		= "connected";
	const MILEAGEONLY 		= 'mileage';
	const HIGHLIGHTED 		= 'highlighted';
	const ID 							= 'id';
	const DEALER_ID 			= 'dealer_id';

}
