<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;

class OperatorEnum extends Enum {
	const OPERATOR_AND = "OPERATOR_AND";
	const OPERATOR_OR  = "OPERATOR_OR";
}