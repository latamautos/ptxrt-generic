<?php
/**
 * Created by PhpStorm.
 * User: mono
 * Date: 28/1/16
 * Time: 11:01 AM
 */

namespace Latamautos\Ptxrt\Utils\Enum;


use MabeEnum\Enum;

class SpanishMonthsEnum extends Enum
{
    const ENERO = 1;
    const FEBRERO = 2;
    const MARZO = 3;
    const ABRIL = 4;
    const MAYO = 5;
    const JUNIO = 6;
    const JULIO = 7;
    const AGOSTO = 8;
    const SEPTIEMBRE = 9;
    const OCTUBRE = 10;
    const NOVIEMBRE = 11;
    const DICIEMBRE = 12;
}