<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;

class MessageCodeEnum extends Enum {

	const ERROR_TESTS_MESSAGE = 'Mensaje de error';
	const ERROR_INVALID_SEARCH_QUERY_PARAM_DTO_ARRAY_MESSAGE = 'SearchQueryParamDTOArray is Invalid';
	const SUCCESS_MESSAGE = 'SUCCESS_MESSAGE';
}
