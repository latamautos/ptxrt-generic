<?php
/**
 * Created by PhpStorm.
 * User: haroldportocarrero
 * Date: 20/1/16
 * Time: 14:53
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;

class DataLayerEnum extends Enum{

	const DATA_LAYER = 'dataLayer';
	const USER_ID = 'userId';
	const SITIO = 'sitio';
	const PAIS = 'pais';
	const SECCION = 'seccion';
	const SUBSECCION = 'subseccion';
	const TIPO = 'tipo';
	const SUBTIPO = 'subtipo';
	const MARCA = 'marca';
	const MODELO = 'modelo';
	const EVENT = 'event';
	const NOMBRE_VIDEO = 'nombreVideo';
	const SCROLL_MOMENTO = 'scrollMomento';
	const SCROLL_CATEGORIA = 'scrollCategoria';
	const SCROLL_ACTION = 'scrollAction';
	const SCROLL_SUBTIPO = 'scrollSubtipo';
	const SCROLL_MARCA = 'scrollMarca';
	const SCROLL_MODELO = 'scrollModelo';
	const SCROLL_ANHO = 'scrollAnho';
	const SOCIAL_OUTBOUND = 'socialOutbound';
	const COMPARTIR_CATEGORIA = 'compartirCategoria';
	const COMPARTIR_MARCA = 'compartirMarca';
	const COMPARTIR_MODELO = 'compartirModelo';
	const COMPARTIR_ANHO = 'compartirAnho';
	const TIPO_REGISTRO = 'tipoRegistro';
	const VERIFICAR_SUBTIPO = 'verificarSubTipo';
	const VERIFICAR_MARCA = 'verificarMarca';
	const VERIFICAR_MODELO = 'verificarModelo';
	const VERIFICAR_ANHO = 'verificarAnho';
	const BUSQUEDA_CATEGORIA = 'busquedaCategoria';
	const BUSQUEDA_MARCA = 'busquedaMarca';
	const BUSQUEDA_SUBTIPO = 'busquedaSubTipo';
	const BUSQUEDA_MODELO = 'busquedaModelo';
	const BUSQUEDA_PRECIO_INICIO = 'busquedaPrecioInicio';
	const BUSQUEDA_PRECIO_FINAL = 'busquedaPrecioFinal';
	const BUSQUEDA_UBICACION = 'busquedaUbicacion';
	const BUSQUEDA_TIPO_DUENO = 'busquedaTipoDueno';
	const ECOMMERCE = 'ecommerce';
	const IMPRESSIONS = 'impressions';
	const NAME = 'name';
	const ID = 'id';
	const PRICE = 'price';
	const BRAND = 'brand';
	const CATEGORY = 'category';
	const VARIANT = 'variant';
	const LIST_DL = 'list';
	const POSITION = 'position';
	const DIMENSION1 = 'dimension1';
	const DIMENSION2 = 'dimension2';
	const DIMENSION3 = 'dimension3';
	const DIMENSION4 = 'dimension4';
	const DIMENSION5 = 'dimension5';
	const CLICK = 'click';
	const PRODUCTS = 'products';
	const SORT_CATEGORY = 'sortCategory';
	const SORT_ACTION = 'sortAction';
	const SORT_MARCA = 'sortMarca';
	const SORT_SUBTIPO = 'sortSubTipo';
	const SORT_MODELO = 'sortModelo';
	const SORT_ANHO = 'sortAnho';

}