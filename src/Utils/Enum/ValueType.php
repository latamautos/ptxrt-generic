<?php
/**
 * Created by PhpStorm.
 * User: eavinti
 * Date: 17/11/15
 * Time: 18:27
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;

class ValueType extends Enum{
	const STRING 	= 'STRING';
	const NUMERIC = 'NUMERIC';
}