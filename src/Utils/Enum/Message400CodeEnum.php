<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Enum;

class Message400CodeEnum extends MessageCodeEnum {

    const ERROR_TESTS_MESSAGE = 'Mensaje de error';
}