<?php
/**
 * Created by PhpStorm.
 * User: eavinti
 * Date: 17/11/15
 * Time: 18:27
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;


class SessionKeysEnum extends Enum{
    const FAVORITE_VEHICLES_KEY = "favoriteVehiclesKey";
    const USER_ID_KEY = "userIdKey";
    const USER_TOKEN_KEY = "userTokenKey";
    const LARAVEL_SESSION_COOKIE = "laravel_session";
}