<?php
/**
 * Created by PhpStorm.
 * User: eavinti
 * Date: 17/11/15
 * Time: 18:27
 */

namespace Latamautos\Ptxrt\Utils\Enum;

use MabeEnum\Enum;


class AggTypeEnum extends Enum{
	const ALL = 'ALL';
	const RANGE  = 'RANGE';
	const BOOLEAN  = 'BOOLEAN';
	const INPUT_RANGE  = 'INPUT_RANGE';
}