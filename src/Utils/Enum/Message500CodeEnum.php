<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Enum;

class Message500CodeEnum extends MessageCodeEnum {

    const ERROR_TESTS_MESSAGE = 'Mensaje de error';
    const USER_PASSWORD_INCORRECT = 'Usuario o Password incorrecto';
    const USER_NOT_CREATED = "Usuario no creado";
}