<?php
/**
 * Created by PhpStorm.
 * User: eavinti
 * Date: 11/11/15
 * Time: 15:24
 */

namespace Latamautos\Ptxrt\Utils\DTO;


class FavoriteDTO
{

    private $userId;
    private $vehicleId;
    private $note;

    function __construct($userId, $vehicleId, $note)
    {
        $this->userId = $userId;
        $this->vehicleId = $vehicleId;
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getVehicleId()
    {
        return $this->vehicleId;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }


}