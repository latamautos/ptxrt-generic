<?php
/**
 * Created by PhpStorm.
 * User: eavinti
 * Date: 11/11/15
 * Time: 15:24
 */

namespace Latamautos\Ptxrt\Utils\DTO;


class LoginDTO
{

    private $email;
    private $password;

    function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }



}