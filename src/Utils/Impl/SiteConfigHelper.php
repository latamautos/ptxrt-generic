<?php
/**
 * Created by PhpStorm.
 * User: haroldportocarrero
 * Date: 4/1/16
 * Time: 18:23
 */

namespace Latamautos\Ptxrt\Utils\Impl;


use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;

class SiteConfigHelper {

	const PATH_SITEALIASES_FILTERS = 'siteConfig.sites';
	const ALIASES = 'aliases';
	const SITES_KEY = 'sites';
	const SITE_ALIASES_FILE_PATH = '../config/siteConfig.php';


	const COUNTRY = 'country';

    const TIMEZONE_KEY = 'timezone';

    const OLD_PTX_SERVER_KEY = 'old_ptx_server';

	public function getAliasesFromSiteAliasesArray() {
		$sitesArray = array ();
		$siteConfigArray = $this->getSitesTypeArray();
		foreach ($siteConfigArray as $siteConfigKey => $siteConfigValue) {
			$sitesArray[$siteConfigKey] = $siteConfigValue[self::ALIASES];
		}
		return $sitesArray;
	}

	public function getSiteByAlias($siteAlias) {
		$sites = $this->getAliasesFromSiteAliasesArray();
		foreach ($sites as $site => $aliases) {
			foreach ($aliases as $alias) {
				if ($siteAlias == $alias) return $site;
			}
		}
		return $siteAlias;
	}

    public function getCountryBySite($site){
        $country = CharacterEnum::CHAR_EMPTY;
        $siteConfigArray = $this->getSitesTypeArray();
        foreach ($siteConfigArray as $siteConfigKey => $siteConfigValue) {
            if($siteConfigKey == $site) return $siteConfigValue[self::COUNTRY];
        }
        return $country;
    }

    public function getTimeZoneBySite($site){
        $country = CharacterEnum::CHAR_EMPTY;
        $siteConfigArray = $this->getSitesTypeArray();
        foreach ($siteConfigArray as $siteConfigKey => $siteConfigValue) {
            if($siteConfigKey == $site) return $siteConfigValue[self::TIMEZONE_KEY];
        }
        return $country;
    }

    public function getOldPTXServerBySite($site){
        $country = CharacterEnum::CHAR_EMPTY;
        $siteConfigArray = $this->getSitesTypeArray();
        foreach ($siteConfigArray as $siteConfigKey => $siteConfigValue) {
            if($siteConfigKey == $site) return $siteConfigValue[self::OLD_PTX_SERVER_KEY];
        }
        return $country;
    }

	private function getSitesTypeArray() {
		$array = include(self::SITE_ALIASES_FILE_PATH);
		return $array[self::SITES_KEY];
	}
}