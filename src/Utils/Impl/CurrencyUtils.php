<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 1/13/16
 * Time: 11:25 AM
 */

namespace Latamautos\Ptxrt\Utils\Impl;


class CurrencyUtils
{

    public static function ptxPriceCurrencyFormat($number)
    {
        return number_format($number, 0, ',', '.');
    }

}