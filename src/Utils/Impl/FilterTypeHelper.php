<?php
/**
 * Created by PhpStorm.
 * User: haroldportocarrero
 * Date: 4/1/16
 * Time: 18:23
 */

namespace Latamautos\Ptxrt\Utils\Impl;


class FilterTypeHelper {

	const PATH_FILTERTYPE_FILTERS = 'filtertype.filters';
	const RELEVANCE = 'relevance';

	public function sanitizeKeyValuePairDtoArray($keyValuePairDtoArray){
		$validKeyValuePairDtoArray = array();
		$filterTypeArray = $this->getFilterTypeArray();
		$keysFilterTypeArray = $this->getKeysFromFilterTypeArray($filterTypeArray);
		foreach($keyValuePairDtoArray as $keyValuePairDto){
			if(!in_array($keyValuePairDto->getName(),$keysFilterTypeArray)) continue;
			$validKeyValuePairDtoArray[] = $keyValuePairDto;
		}
		return $validKeyValuePairDtoArray;
	}

	public function getBootsFromFilterTypeArray(){
		$bootsArray = array();
		$filterTypeArray = $this->getFilterTypeArray();
		foreach($filterTypeArray as $filterTypeKey => $filterTypeValue){
			$bootsArray[$filterTypeKey] = $filterTypeValue[self::RELEVANCE];
		}
		return $bootsArray;
	}

	public function getFilterTypeByName($name){
		return \Config::get(self::PATH_FILTERTYPE_FILTERS . '.' . $name);
	}

	private function getKeysFromFilterTypeArray($filterTypeArray){
		$keysFilterTypeArray = array();
		foreach($filterTypeArray as $filterTypeKey => $filterTypeValue){
			$keysFilterTypeArray[] = $filterTypeKey;
		}
		return $keysFilterTypeArray;
	}

	private function getFilterTypeArray(){
		return \Config::get(self::PATH_FILTERTYPE_FILTERS);
	}

}