<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 1/13/16
 * Time: 11:25 AM
 */

namespace Latamautos\Ptxrt\Utils\Impl;


use DateTime;
use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;
use Latamautos\Ptxrt\Utils\Enum\SpanishMonthsEnum;

class DateUtils
{

    public static function timeElapsedString($stringDatetime, $timeZone, $full = false)
    {
        date_default_timezone_set($timeZone);
        $now = new DateTime;
        $ago = new DateTime($stringDatetime);
        $diff = $now->diff($ago);

        if ($diff->days > 14) {
            return "hace más de 15 días";
        }

        $string = array(
            'days' => 'dia',
            'h' => 'hora',
            'i' => 'minuto',
            's' => 'segundo',
        );

        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? 'hace ' . implode(', ', $string) : 'justo ahora';
    }

    public static function formatStringDate($stringDatetime, $format = 'd-m-y')
    {
        $date = new DateTime($stringDatetime);
        return $date->format($format);
    }

    public static function getSpanishDate($stringDate) {
        $date = strtotime($stringDate);
        return SpanishMonthsEnum::get(intval(date('n', $date))).CharacterEnum::SPACE.date('j Y', $date);
    }

}