<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 1/13/16
 * Time: 11:25 AM
 */

namespace Latamautos\Ptxrt\Utils\Impl;


class StringUtils
{

    public static function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

}