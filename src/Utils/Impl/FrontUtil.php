<?php
/**
 * Created by PhpStorm.
 * User: denMac
 * Date: 14/12/15
 * Time: 3:48 PM
 */

namespace Latamautos\Ptxrt\Utils\Impl;

class FrontUtil{

	const BEIGNNING_PATH_FOTOS_NGINX = "https://images.latamautos.com/thumbs/w/";
	const CUT_FOTOS_NGINX = "xC";

	public static function getSince($since) {

		$dateA  = strtotime("now");
		$dateB  = strtotime($since);
		$text	= '';

		$diff = $dateA - $dateB;

		$d = floor($diff / 86400);
		$h = floor(($diff - ($d * 86400)) / 3600);
		$m = floor(($diff - ($d * 86400) - ($h * 3600)) / 60);
		$s = $diff % 60;

		if($d>0){
			if($d==1){
				$text .= 'un día';
			}
			elseif($d>15){
				$text .= '+ de 15 días';
			}
			else{
				$text .= $d.' días';
			}
		}
		elseif($h>0){
			if($h==1){
				$text .= 'una hora';
			}
			else{
				$text .= $h.' horas';
			}
		}
		else{
			if($m==1){
				$text .= 'un minuto';
			}
			else{
				$text .= $m.' minutos';
			}
		}
		return $text;
	}

	public static function truncateStr($str, $limit, $end = "...") {
		$str = strip_tags($str);
		return substr($str, 0, $limit) . $end;
	}

}