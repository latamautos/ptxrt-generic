<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Impl;


class RequestManager {


	public function persistDataOnList($data, $listName) {
		if (empty($data)) return;
		$dataList = $this->getListByName($listName);
		$dataList[] = $data;
		$_REQUEST[$listName] = $dataList;
	}

	public function getListByName($listName) {
		if (!isset($_REQUEST[$listName])) {
			$_REQUEST[$listName] = [];
		}
		return $_REQUEST[$listName];
	}

	public function clearListByName($listName) {
		$_REQUEST[$listName] = array ();
	}
}