<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Impl;

use Latamautos\Ptxrt\Generic\DistributedServices\Interceptors\PageRequestInterceptor;
use Illuminate\Support\Facades\App;
use Latamautos\Ptxrt\Generic\Domain\Enum\RequestUserDataKeysEnum;

class BaseService {

	private $notification;
	private $pageRequest;
	private $userDataRequest;

	public static function getClassName() {
		return __CLASS__;
	}

	public function getNotifications() {
		if(!$this->notification){
			$this->notification = app(Notification::getClassName());
		}
		return $this->notification;
	}

	public function setNotification(){
		return $this->notification;
	}

	public function getPageRequest() {
		$requestManager = new RequestManager();
		$pageRequest = $requestManager->getListByName(PageRequestInterceptor::PAGE_REQUEST_KEY);
		if($pageRequest) $this->pageRequest = $pageRequest[0];
		$this->setSiteInPageRequest();
		return $this->pageRequest;
	}

	public function setPageRequest(){
		return $this->pageRequest;
	}

	public function getUserDataRequest() {
		$requestManager = new RequestManager();
		$userDataRequest = $requestManager->getListByName(RequestUserDataKeysEnum::USER_DATA);
		if($userDataRequest) $this->userDataRequest = $userDataRequest[0];
		return $this->userDataRequest;
	}

    public function getRemoteServer(){
        $siteConfigHelper = new SiteConfigHelper();
        return $siteConfigHelper->getOldPTXServerBySite($this->getPageRequest()->getSite());
    }

	private function setSiteInPageRequest() {
		$userDataRequest = $this->getUserDataRequest();
		if ($userDataRequest) {
			$site = $userDataRequest->getDomain();
			$siteConfigHelper = new SiteConfigHelper();
			$this->pageRequest->setSite($siteConfigHelper->getSiteByAlias($site));
		}
	}
}
