<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils\Impl;

use Latamautos\Ptxrt\Utils\Enum\Message500CodeEnum;
use Latamautos\Ptxrt\Utils\Enum\MessageCodeEnum;
use ManagerRequerstMessage;

class Notification
{

    const ERROR_MESSAGES_KEY = "errorMessages";
    const WARNING_MESSAGES_KEY = "warningMessages";
    const SUCCESS_MESSAGES_KEY = "successMessages";
    const LINKED_MESSAGES_KEY = "linkedMessages";

    private $hasErrors;
    private $statusCode;
    private $managerRequest;

    function __construct(RequestManager $managerRequest)
    {
        $this->managerRequest = $managerRequest;
    }

    public static function getClassName()
    {
        return __CLASS__;
    }

    public function clear()
    {
        $this->managerRequest->clearListByName(self::ERROR_MESSAGES_KEY);
        $this->managerRequest->clearListByName(self::WARNING_MESSAGES_KEY);
        $this->managerRequest->clearListByName(self::SUCCESS_MESSAGES_KEY);
        $this->managerRequest->clearListByName(self::LINKED_MESSAGES_KEY);
    }

    public function hasErrors()
    {
        return !empty($this->managerRequest->getListByName(self::LINKED_MESSAGES_KEY)) || !empty($this->managerRequest->getListByName(self::ERROR_MESSAGES_KEY));
    }

    public function getErrorMessages()
    {
        return $this->managerRequest->getListByName(self::ERROR_MESSAGES_KEY);
    }

    public function getWarningMessages()
    {
        return $this->managerRequest->getListByName(self::WARNING_MESSAGES_KEY);
    }

    public function getSuccessMessages()
    {
        return $this->managerRequest->getListByName(self::SUCCESS_MESSAGES_KEY);
    }

    public function getLinkedMessages()
    {
        return $this->managerRequest->getListByName(self::LINKED_MESSAGES_KEY);
    }

    public function addError(MessageCodeEnum $messageCode)
    {
        if (!$messageCode) return;
        $this->managerRequest->persistDataOnList($messageCode->getName(), self::ERROR_MESSAGES_KEY);
    }

    public function addLinkedError(MessageCodeEnum $messageCode, $reference)
    {
        if (!$messageCode) return;
        $data[$reference] = $messageCode->getName();
        $this->managerRequest->persistDataOnList($data, self::LINKED_MESSAGES_KEY);
    }

      public function addWarning(MessageCodeEnum $messageCode)
    {
        if (!$messageCode) return;
        $this->managerRequest->persistDataOnList($messageCode->getName(), self::WARNING_MESSAGES_KEY);
    }

    public function addSuccess(MessageCodeEnum $messageCode)
    {
        if (!$messageCode) return;
        $this->managerRequest->persistDataOnList($messageCode->getName(), self::SUCCESS_MESSAGES_KEY);
    }

    public function getMessages()
    {
        $data = array();
        $data[Notification::ERROR_MESSAGES_KEY] = $this->getErrorMessages();
        $data[Notification::WARNING_MESSAGES_KEY] = $this->getWarningMessages();
        $data[Notification::SUCCESS_MESSAGES_KEY] = $this->getSuccessMessages();
        $data[Notification::LINKED_MESSAGES_KEY] = $this->getLinkedMessages();
        return $data;
    }

    public function getStatusCode()
    {
        if (!$this->hasErrors()) return 200;
        return $this->getErrorCode();
    }

    private function getErrorCode()
    {
        $array500Errors = Message500CodeEnum::getConstants();
        $errorKey = '';
        if (!empty($this->getLinkedMessages())) {
            $errorKey = $this->getLinkedMessages();
        }
        if (!empty($this->getErrorMessages())) {
            $errorKey = $this->getErrorMessages()[0];
        }
        return (array_key_exists($errorKey, $array500Errors) ? 500 : 400);
    }
}
