<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Utils;


class KeyValuesPair {

	private $key;
	private $values;

	function __construct($key, $values) {
		$this->key = $key;
		$this->values = $values;
	}

	public function getKey() {
		return $this->key;
	}

	public function setKey($key) {
		$this->key = $key;
	}

	public function getValues() {
		return $this->values;
	}

	public function setValues($values) {
		$this->values = $values;
	}

	public function addValue($value) {
		$this->values[] = $value;
	}

	public function addValues($values) {
		foreach($values as $value ){
			if(!in_array($value,$this->values)) $this->values[] = $value;
		}
	}

	public function existThisKey($key){
		return array_key_exists($key,$this->values);
	}

	public function removeItemByKey($key){
		unset($this->values[$key]);
		return $this->values;
	}

}