<?php
/**
 * Created by PhpStorm.
 * User: Marco Guillen
 * Date: 28/09/15
 * Time: 11:31
 */

namespace Latamautos\Ptxrt\Utils\Impl;

class SocketRequestUtil
{

    /* *
     * Do HTTP Request based on Sockets
     * @return void
     */
    static function request($url, $requestData, $requestMethod = 'POST', $requestAuth = '', $requestSynchronousTimeOut = '3', $doSynchronous = false)
    {
        // Parse url to Request
        $requestMethod = strtoupper($requestMethod);
        $urlParts = parse_url($url);

        // Create socket client for transport
        $socketClient = fsockopen($urlParts['host'], isset($urlParts['port']) ? $urlParts['port'] : 80, $eNo, $eStr, $requestSynchronousTimeOut);

        // Create HTTP Request Header
        $out = "$requestMethod " . @$urlParts['path'] . "?" . @$urlParts['query'] . " HTTP/1.1\r\n";
        $out .= "Accept: application/json\r\n";
        $out .= "Content-Encoding: identity\r\n";

        $json = json_encode($requestData);

        $out .= "Content-Type: application/json\r\n";
        if ($requestAuth != '') {
            $requestAuth = base64_encode($requestAuth);
            $out .= "Authorization: Basic " . $requestAuth . "\r\n";
        }
        $out .= "Content-Length: " . strlen($json) . "\r\n";
        $out .= "Host: " . $urlParts['host'] . "\r\n";
        $out .= "Connection: Close\r\n\r\n";
        $out .= $json . "\r\n";

        // Write the HTTP Request Header over the socket
        fwrite($socketClient, $out);
        // Make request synchronous
        $result = "";
        if ($doSynchronous) {
            stream_set_blocking($socketClient, true);
            list($header, $body) = explode("\r\n\r\n", stream_get_contents($socketClient), 2);
            $result = substr($body,strpos($body, '{'), -7);
        }
        // Close the socket
        fclose($socketClient);
        return $result;
    }

}