<?php
namespace Latamautos\Ptxrt\Util\Impl;

use Latamautos\Ptxrt\Generic\Domain\Contract\ILoginHandler;
use Latamautos\Ptxrt\Generic\Infrastructure\Impl\LoginRepository;
use Latamautos\Ptxrt\Utils\Enum\Message500CodeEnum;
use Latamautos\Ptxrt\Utils\Enum\MessageCodeEnum;
use Latamautos\Ptxrt\Utils\Enum\SessionKeysEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;
use Zend\Authentication\Storage\Session;

class SessionUtil
{

    public static function isUserLoggedIn()
    {
        $sessionImpl = new SessionImpl();
        $userId = $sessionImpl->get(SessionKeysEnum::USER_ID_KEY);
        return isset($userId);
    }

}