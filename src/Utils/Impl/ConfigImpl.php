<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 12/3/15
 * Time: 11:41 AM
 */

namespace Latamautos\Ptxrt\Utils\Impl;

use Illuminate\Support\Facades\Config;

class ConfigImpl {

	public function get($key){
		return Config::get($key);
	}
}