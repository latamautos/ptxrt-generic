<?php
/**
 * Created by PhpStorm.
 * User: hvelazquez@latamautos.com
 * Date: 14/1/16
 * Time: 19:13
 */

namespace Latamautos\Ptxrt\Utils\Impl;

class SearchFilterValue {

	private $name;
	private $values;
	private $selected;
	private $count;
	private $class;
	private $nameSearch;

	public function __construct($name,$values,$selected,$count,$nameSearch, $class = "") {
		$this->name = $name;
		$this->value = $values;
		$this->selected = $selected;
		$this->count = $count;
		$this->class = $class;
		$this->nameSearch = $nameSearch;
	}

	public function getCount() {
		return $this->count;
	}

	public function setCount($count) {
		$this->count = $count;
	}

	public function getName($lower = false) {
		if($lower)
			return strtolower($this->name);
		return $this->name;
	}


	public function setName($name) {
		$this->name = $name;
	}


	public function getValues() {
		return $this->value;
	}

	public function setValues($values) {
		$this->values = $values;
	}

	public function getSelected() {
		return $this->selected;
	}

	public function setSelected($selected) {
		$this->selected = $selected;
	}

	public function getClass() {
		return $this->class;
	}

	public function setClass($class) {
		$this->class = $class;
	}

	public function getNameSearch() {
		return $this->nameSearch;
	}

	public function setNameSearch($nameSearch) {
		$this->nameSearch = $nameSearch;
	}

}