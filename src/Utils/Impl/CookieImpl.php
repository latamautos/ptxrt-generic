<?php
/**
 * Created by PhpStorm.
 * User: mono
 * Date: 4/2/16
 * Time: 12:50 PM
 */

namespace Latamautos\Ptxrt\Utils\Impl;


use Illuminate\Support\Facades\Cookie;

class CookieImpl
{
    public function get($key)
    {
        return Cookie::get($key);
    }
}