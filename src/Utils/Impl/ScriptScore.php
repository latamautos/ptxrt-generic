<?php
/**
 * Created by PhpStorm.
 * User: haroldportocarrero
 * Date: 16/2/16
 * Time: 14:26
 */

namespace Latamautos\Ptxrt\Utils\Impl;


use Latamautos\Ptxrt\Search\Domain\Enum\ElasticsearchKeyEnum;

class ScriptScore {

	const ES_SCRIPT_SCORE_MAX_PLAN					= 'ES_SCRIPT_SCORE_MAX_PLAN';
	const ES_SCRIPT_SCORE_MAX_SCORE_PLAN			= 'ES_SCRIPT_SCORE_MAX_SCORE_PLAN';
	const ES_SCRIPT_SCORE_MAX_SCORE_CONNECTED		= 'ES_SCRIPT_SCORE_MAX_SCORE_CONNECTED';
	const ES_SCRIPT_SCORE_MAX_DAYS_CONNECTED		= 'ES_SCRIPT_SCORE_MAX_DAYS_CONNECTED';
	const ES_SCRIPT_SCORE_MAX_SCORE_CONFIRMED		= 'ES_SCRIPT_SCORE_MAX_SCORE_CONFIRMED';
	const ES_SCRIPT_SCORE_MAX_DAYS_CONFIRMED		= 'ES_SCRIPT_SCORE_MAX_DAYS_CONFIRMED';
	const ES_SCRIPT_SCORE_VALUE_SUBSTRACT_SCORE		= 'ES_SCRIPT_SCORE_VALUE_SUBSTRACT_SCORE';
	const ES_SCRIPT_SCORE_DEALER_VALUE				= 'ES_SCRIPT_SCORE_DEALER_VALUE';
	const ES_SCRIPT_SCORE_USER_VALUE				= 'ES_SCRIPT_SCORE_USER_VALUE';
	const ES_SCRIPT_SCORE_FACTOR					= 'ES_SCRIPT_SCORE_FACTOR';
	const ES_SCRIPT_SCORE_RESULT					= 'ES_SCRIPT_SCORE_RESULT';
	const ES_SCRIPT_SCORE_FORMULA					= 'ES_SCRIPT_SCORE_FORMULA';

	private function getScriptScore(){
		$formula = env(self::ES_SCRIPT_SCORE_FORMULA);
		$params = array(ElasticsearchKeyEnum::MAX_PLAN => (float)env(self::ES_SCRIPT_SCORE_MAX_PLAN),
						ElasticsearchKeyEnum::MAX_SCORE_PLAN => (float)env(self::ES_SCRIPT_SCORE_MAX_SCORE_PLAN),
						ElasticsearchKeyEnum::MAX_SCORE_CONNECTED => (float)env(self::ES_SCRIPT_SCORE_MAX_SCORE_CONNECTED),
						ElasticsearchKeyEnum::MAX_DAYS_CONNECTED => (float)env(self::ES_SCRIPT_SCORE_MAX_DAYS_CONNECTED),
						ElasticsearchKeyEnum::MAX_SCORE_CONFIRMED => (float)env(self::ES_SCRIPT_SCORE_MAX_SCORE_CONFIRMED),
						ElasticsearchKeyEnum::MAX_DAYS_CONFIRMED => (float)env(self::ES_SCRIPT_SCORE_MAX_DAYS_CONFIRMED),
						ElasticsearchKeyEnum::VALUE_SUBSTRACT_SCORE => (float)env(self::ES_SCRIPT_SCORE_VALUE_SUBSTRACT_SCORE),
						ElasticsearchKeyEnum::DEALER_VALUE => (float)env(self::ES_SCRIPT_SCORE_DEALER_VALUE),
						ElasticsearchKeyEnum::USER_VALUE => (float)env(self::ES_SCRIPT_SCORE_USER_VALUE),
						ElasticsearchKeyEnum::FACTOR => (float)env(self::ES_SCRIPT_SCORE_FACTOR),
						ElasticsearchKeyEnum::RESULT => (float)env(self::ES_SCRIPT_SCORE_RESULT));
		$scriptScore[] = array(ElasticsearchKeyEnum::SCRIPT_SCORE => array(ElasticsearchKeyEnum::PARAMS=>$params,ElasticsearchKeyEnum::SCRIPT => $formula));
		return $scriptScore;
	}

	public function getQueryArrayWithFunctionScriptScore($queryArray){
		$queryArray[ElasticsearchKeyEnum::FUNCTIONS] = $this->getScriptScore();
		return $queryArray;
	}

}