<?php
/**
 * Created by PhpStorm.
 * User: Chan
 * Date: 15/11/14
 * Time: 11:31
 */

namespace Latamautos\Ptxrt\Utils\Impl;


use DateTime;

/**
 * This class is also in PTX, if you change anything, you have to change it in PTX also.
 *
 */
class LoginUtil {

    //
    const MAX_SECONDS_VALID_TS = 300;
    const ENCRIPTION_TYPE = "sha256";
    const SALT_KEY="SALT";

    private $userEmail;
    private $ts;

	function __construct($userEmail, $ts) {
        $this->userEmail = $userEmail;
        $this->ts = (float) $ts;
    }

    /**
     * This method, encrypt a hash for login
     *
     * @return string
     */
    public function generate_hash() {
        $hash = hash(self::ENCRIPTION_TYPE, $this->userEmail . $this->ts . env(self::SALT_KEY));
        return $hash;
    }

    public function validTimeStamp() {
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        if (abs($timestamp - $this->ts) <= self::MAX_SECONDS_VALID_TS) {
            return true;
        }
        return false;
    }

    public function validHash($hash){
        if($this->validTimeStamp()){
            return $hash == $this->generate_hash();
        }
        return false;
    }

    public function generateQueryString(){
        return "token=" . $this->generate_hash() . "&userEmail=" . $this->userEmail . "&ts=" . $this->ts;
    }
} 