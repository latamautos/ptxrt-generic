<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 12/3/15
 * Time: 11:41 AM
 */

namespace Latamautos\Ptxrt\Utils\Impl;

use Illuminate\Support\Facades\Session;

class SessionImpl
{

    public function set($key, $value)
    {
        Session::set($key, $value);
    }

    public function get($key)
    {
        return Session::get($key);
    }

}