<?php
/**
 * Created by PhpStorm.
 * User: juandiego
 * Date: 12/16/15
 * Time: 3:13 PM
 */

namespace Latamautos\Ptxrt\Utils\Impl;


class Order {

	private $value;
	private $direction;

	public function __construct($value, $direction) {
		$this->value = $value;
		$this->direction = $direction;
	}

	public function getValue() {
		return $this->value;
	}

	public function setValue($value) {
		$this->value = $value;
	}

	public function getDirection() {
		return $this->direction;
	}

	public function setDirection($direction) {
		$this->direction = $direction;
	}

}