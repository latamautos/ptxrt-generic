<?php
/**
 * Created by PhpStorm.
 * User: eavinti
 * Date: 01/02/16
 * Time: 3:18 PM
 */

namespace Latamautos\Ptxrt\Utils\Impl;

use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;

class ImageHelper {

	const PATH_FOTOS_NGINX = "//images.latamautos.com/thumbs/w/";
	const CUT_FOTOS_NGINX = "xC";

	public static function resize($imageLink, $width, $height=null, $cut=true) {
		$fotosNginx = self::PATH_FOTOS_NGINX . $width;
		if($height != null ){
			$fotosNginx .= CharacterEnum::X_LOWERCASE . $height;
		}
		if($cut == true ){
			$fotosNginx .= self::CUT_FOTOS_NGINX;
		}
		$fotosNginx .= CharacterEnum::SLASH . $imageLink;
		return $fotosNginx;
	}

}