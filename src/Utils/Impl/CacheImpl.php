<?php
/**
 * Created by PhpStorm.
 * User: amstronghuang
 * Date: 12/3/15
 * Time: 11:41 AM
 */

namespace Latamautos\Ptxrt\Utils\Impl;


use Illuminate\Support\Facades\Cache;
use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;

class CacheImpl {

	public function cacheForever($key, $value){
		$key = strtolower(str_replace(CharacterEnum::SPACE, CharacterEnum::PLUS,$key));
		Cache::forever($key, $value);
	}

	public function cacheHas($key){
		$key = strtolower(str_replace(CharacterEnum::SPACE, CharacterEnum::PLUS,$key));
		return Cache::has(strtolower($key));
	}

	public function cacheGet($key){
		$key = strtolower(str_replace(CharacterEnum::SPACE, CharacterEnum::PLUS,$key));
		return Cache::get(strtolower($key));
	}

	public function cacheFlush(){
		Cache::flush();
	}
}