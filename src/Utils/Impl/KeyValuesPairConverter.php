<?php
/**
 * Created by PhpStorm.
 * User: eavinti
 * Date: 11/11/15
 * Time: 15:06
 */

namespace Latamautos\Ptxrt\Utils\Impl;


use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;
use Latamautos\Ptxrt\Utils\KeyValuesPair;

class KeyValuesPairConverter {

	const SQUARE_BRACKET_START 	= "[";
	const SQUARE_BRACKET_END 		= "]";
	const AMPERSAND 						= '&';
	const EQUALS		 						= "=";

	public function parseQueryStringToArray($queryString) {
		$queryStringArray = array ();
		$queryString = urldecode($queryString);
		$brackets = array (self::SQUARE_BRACKET_START, self::SQUARE_BRACKET_END);
		$queryString = str_replace($brackets, "", $queryString);
		$filters = explode(self::AMPERSAND, $queryString);
		$queryStringArray = $this->sanitizeQueryStringArray($filters, $queryStringArray);
		return $queryStringArray;
	}

	public function sanitizeQueryStringArray($keyValueStringArray, $queryStringArray) {
		foreach ($keyValueStringArray as $keyValuePairString) {
			if($keyValuePairString != CharacterEnum::CHAR_EMPTY && strpos($keyValuePairString, self::EQUALS)===false) $keyValuePairString = "$keyValuePairString=$keyValuePairString";
			$keyValuePairArray = explode(self::EQUALS, $keyValuePairString);
			$newParamValues = $this->getQueryParamValues($queryStringArray, $keyValuePairArray);
			if (!empty($newParamValues))
				$queryStringArray[$keyValuePairArray[0]] = $this->getQueryParamValues($queryStringArray, $keyValuePairArray);
		}
		return $queryStringArray;
	}

	public function convertToArray($queryString) {
		if (empty($queryString)) return [];
		$keyValuePairDtoArray = array ();
		$queryStringArray = $this->parseQueryStringToArray($queryString);
		foreach ($queryStringArray as $queryStringParamName => $queryStringParamValue) {
			$keyValuePairDtoArray[] = new KeyValuesPair($queryStringParamName, $queryStringParamValue);
		}
		return $keyValuePairDtoArray;
	}

	public function paramKeyExists($break, $queryStringArray) {
		return array_key_exists($break[0], $queryStringArray);
	}

	public function isNotDuplicatedParamValue($break, $queryStringArray) {
		if(!isset($break[0]) ||  !isset($break[1]))  throw new \Exception("Bad query string");
		return !in_array($break[1], $queryStringArray[$break[0]]);
	}

	public function getQueryParamValues($queryStringArray, $keyValuePair) {
		if (!$this->paramKeyExists($keyValuePair, $queryStringArray) && isset($keyValuePair[1]))
			return array ($keyValuePair[1]);
		if ($this->isNotDuplicatedParamValue($keyValuePair, $queryStringArray))
			return array_merge($queryStringArray[$keyValuePair[0]], array ($keyValuePair[1]));
	}
}
