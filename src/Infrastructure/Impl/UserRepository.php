<?php

namespace Latamautos\Ptxrt\Generic\Infrastructure\Impl;

use Latamautos\Ptxrt\Generic\Infrastructure\Contract\IUserRepository;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;
use Latamautos\Ptxrt\Utils\Impl\SocketRequestUtil;

class UserRepository extends BaseService implements IUserRepository
{

    const EMAIL_ARRAY_KEY = "email";
    const USERNAME_ARRAY_KEY = "userName";
    const PASSWORD_ARRAY_KEY = "password";
    const FIRST_NAME_ARRAY_KEY = "firstName";
    const MOBILE_NUMBER_ARRAY_KEY = "mobileNumber";
    const SUBSCRIBE_NEWSLETTER_ARRAY_KEY = "suscribeNewsletter";

    const REMOTE_SIGN_UP_ENDPOINT = "/ptx/api/v2/user/signUp";

    const REMOTE_RECOVER_PASSWORD_ENDPOINT = "/ptx/api/v2/user/recover-password";

    const METHOD_POST = "POST";

    const TIMEOUT = 2;

    private $sessionImpl;

    function __construct(SessionImpl $sessionImpl)
    {
        $this->sessionImpl = $sessionImpl;
    }

    public function callRemoteSignUp($email, $password, $name, $cellphone = "00", $subscribeNewsletter = 1)
    {
        $requestData = array(
            self::USERNAME_ARRAY_KEY => $email,
            self::PASSWORD_ARRAY_KEY => $password,
            self::FIRST_NAME_ARRAY_KEY => $name,
            self::MOBILE_NUMBER_ARRAY_KEY => $cellphone,
            self::SUBSCRIBE_NEWSLETTER_ARRAY_KEY => $subscribeNewsletter
        );
        return SocketRequestUtil::request($this->getRemoteSignUpEndpoint(), $requestData, self::METHOD_POST, '', self::TIMEOUT, true);
    }

    public function callRemoteRecoverPassword($email)
    {
        $requestData = array(
            self::EMAIL_ARRAY_KEY => $email
        );
        return SocketRequestUtil::request($this->getRemoteRecoverPasswordEndpoint(), $requestData, self::METHOD_POST);
    }

    private function getRemoteRecoverPasswordEndpoint()
    {
        return $this->getRemoteServer() . self::REMOTE_RECOVER_PASSWORD_ENDPOINT;
    }

    private function getRemoteSignUpEndpoint()
    {
        return $this->getRemoteServer() . self::REMOTE_SIGN_UP_ENDPOINT;
    }

}