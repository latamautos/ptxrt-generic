<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Infrastructure\Impl;

use Latamautos\Ptxrt\Generic\Infrastructure\Contract\IFavoriteRepository;
use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;
use Latamautos\Ptxrt\Utils\Enum\SessionKeysEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;
use Latamautos\Ptxrt\Utils\Impl\SocketRequestUtil;

/**
 * @property  sessionImpl
 */
class FavoriteRepository extends BaseService implements IFavoriteRepository
{

    const USER_ID_ARRAY_KEY = "userId";
    const VEHICLE_ID_ARRAY_KEY = "vehicleId";
    const NOTE_ARRAY_KEY = "note";

    const REMOTE_REMOVE_FAVORITE_ENDPOINT = "/ptx/api/v2/bookmark/remove";
    const REMOTE_ADD_FAVORITE_ENDPOINT = "/ptx/api/v2/bookmark/add";
    const REMOTE_LIST_FAVORITES_ENDPOINT = "/ptx/api/v2/user/search-bookmarks/";

    const USER_ID_QUERY_STRING = "userId";
    const TOKEN_QUERY_STRING = "token";
    const METHOD_GET = "GET";
    const TIMEOUT = 5;

    private $sessionImpl;

    function __construct(SessionImpl $sessionImpl)
    {
        $this->sessionImpl = $sessionImpl;
    }

    public function callRemoteAddFavorite($vehicleId)
    {
        $requestData = array(
            self::USER_ID_ARRAY_KEY => $this->sessionImpl->get(SessionKeysEnum::USER_ID_KEY),
            self::VEHICLE_ID_ARRAY_KEY => $vehicleId,
            self::NOTE_ARRAY_KEY => CharacterEnum::CHAR_EMPTY
        );
        SocketRequestUtil::request($this->getRemoteAddFavoriteEndpoint(), $requestData);
    }

    public function callRemoteRemoveFavorite($vehicleId)
    {
        $requestData = array(
            self::USER_ID_ARRAY_KEY => $this->sessionImpl->get(SessionKeysEnum::USER_ID_KEY),
            self::VEHICLE_ID_ARRAY_KEY => $vehicleId,
            self::NOTE_ARRAY_KEY => CharacterEnum::CHAR_EMPTY
        );
        SocketRequestUtil::request($this->getRemoteRemoveFavoriteEndpoint(), $requestData);
    }

    public function callRemoteListFavoritesForLoggedInUser()
    {
        $vehicleIdsArray = [];
        $response = SocketRequestUtil::request($this->getRemoteListFavoritesForLoggedInUserEndpoint(), null, self::METHOD_GET, CharacterEnum::CHAR_EMPTY, self::TIMEOUT, true);
        foreach (json_decode($response)->data as $vehicle) {
            $vehicleIdsArray[] = $vehicle->vehicle_id;
        }

        return $vehicleIdsArray;
    }

    private function getRemoteRemoveFavoriteEndpoint()
    {
        return $this->getRemoteServer() . self::REMOTE_REMOVE_FAVORITE_ENDPOINT . $this->getLoggedUserQueryString();
    }

    private function getRemoteAddFavoriteEndpoint()
    {
        return $this->getRemoteServer() . self::REMOTE_ADD_FAVORITE_ENDPOINT . $this->getLoggedUserQueryString();
    }

    private function getRemoteListFavoritesForLoggedInUserEndpoint()
    {
        return $this->getRemoteServer() . self::REMOTE_LIST_FAVORITES_ENDPOINT . $this->sessionImpl->get(SessionKeysEnum::USER_ID_KEY) . $this->getLoggedUserQueryString();
    }

    private function getLoggedUserQueryString()
    {
        return CharacterEnum::QUESTION . self::USER_ID_QUERY_STRING . CharacterEnum::EQUALS . $this->sessionImpl->get(SessionKeysEnum::USER_ID_KEY) . CharacterEnum::CHAR_AND . self::TOKEN_QUERY_STRING . CharacterEnum::EQUALS . $this->sessionImpl->get(SessionKeysEnum::USER_TOKEN_KEY);
    }
}