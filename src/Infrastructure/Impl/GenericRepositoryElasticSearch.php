<?php
/**
 * @author Harold Portocarrero <hportocarrero@latamautos.com>
 * @author Marco Gullen <marco.gullen@latamautos.com>
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */
namespace Latamautos\Ptxrt\Generic\Infrastructure\Impl;

use Doctrine\ORM\Persisters\PersisterException;
use Latamautos\Ptxrt\Generic\Domain\Contract\IFindable;
use Elasticsearch\ClientBuilder;
use Latamautos\Ptxrt\Search\Domain\Enum\ElasticsearchKeyEnum;
use Latamautos\Ptxrt\Search\Domain\Enum\VehicleOrderEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use MkDoctrineSpringData\Pagination\PageImpl;
use MkDoctrineSpringData\Pagination\Sorting\Direction;


abstract class GenericRepositoryElasticSearch extends BaseService implements IFindable{

	protected $client;
	protected $index;
	protected $type;
	protected $pagination = array();
	protected $sort = array();
	protected $totalElements = 0;
	protected $orderByValuesArray = array();

	const INDEX = 'index';
	const TYPE = 'type';
	const BODY = 'body';
	const ID = 'id';
	const ELASTICSEARCH_HOST = 'nosql.connections.elasticsearch.host';
	const ELASTICSEARCH_PORT = 'nosql.connections.elasticsearch.port';
	const SITE = 'site';
	const FROM = 'from';
	const SIZE = 'size';
	const ORDER = 'order';
	const SORT = 'sort';
	const HITS = 'hits';
	const TOTAL = 'total';
	const OPTIONS = "options";
	const SOURCE = 'source';
	const ATTRIBUTES = 'attributes.';
	const VALUE = '.value';
	private $totalPages;

	function __construct() {
		try{
			$this->client = ClientBuilder::create()->setHosts([\Config::get(self::ELASTICSEARCH_HOST).':'.\Config::get(self::ELASTICSEARCH_PORT)])->build();
			$this->getOrderByValuesArray();
		} catch (\Exception $e) {
			throw new PersisterException($e->getMessage());
		}
	}

	abstract function convertToDomainModel($response);

	public function addIndex($index){
		$this->index = $index;
	}

	public function addType($type){
		$this->type = $type;
	}

	public function findById($id) {
		try{
			$response = $this->client->get(array(self::INDEX=>$this->index,
				self::TYPE=>$this->type,
				self::ID=>$id));
			return $this->convertToDomainModel($response);
		} catch (\Exception $e) {
			\Log::error($e->getMessage());
			throw new PersisterException($e->getMessage());
		}
	}

	public function searchDocument($jsonQuery) {
			$query = json_decode($jsonQuery, true);
			if($query == null) throw new PersisterException("Undefined query");
			return $this->searchDocumentFromArray($query);
	}

	public function searchDocumentFromArray($queryArray) {
		try{
			$params = array(self::INDEX=>$this->index,
				self::TYPE=>$this->type,
				self::BODY => $this->sort ? $queryArray + $this->pagination + $this->sort : $queryArray + $this->pagination);
			$response = $this->client->search($params);
			if($response[self::HITS][self::TOTAL]) $this->setTotalElements($response[self::HITS][self::TOTAL]);
			return $this->convertToDomainModel($response);
		} catch (\Exception $e) {
			\Log::error($e->getMessage());
			throw new PersisterException($e->getMessage());
		}
	}

	public function searchDocumentFromArrayPaged($queryArray, $pageRequest = null) {
		return $this->convertResponseToPageImpl($this->searchDocumentFromArray($queryArray), $pageRequest);
	}

	/**
	 * @author Livan Frometa <lfrometa@latamautos.com>
	 */
	public function searchDocumentFromArrayBySite($queryArray) {
		$queryArray = $this->addQuerySite($queryArray);
		return $this->searchDocumentFromArray($queryArray);
	}

	public function searchDocumentFromArrayBySiteAndSource($source, $queryArray) {
		$queryArray = $this->addQuerySource($source, $queryArray);
		return $this->searchDocumentFromArrayBySite($queryArray);
	}

	public function searchDocumentFromArrayBySitePaged($queryArray, $pageRequest = null) {
		return $this->convertResponseToPageImpl($this->searchDocumentFromArrayBySite($queryArray), $pageRequest);
	}

	private function convertResponseToPageImpl($response, $pageRequest){
		$pageImp = new PageImpl($response,$pageRequest ? $pageRequest : $this->getPageRequest(),$this->getTotalElements());
		return $pageImp;
	}

	/**
	 * @author Livan Frometa <lfrometa@latamautos.com>
	 */
	public function searchDocumentFromJsonBySite($jsonQuery) {
		$queryArray = $this->addQuerySite(json_decode($jsonQuery));
		return $this->searchDocumentFromArray(json_encode($queryArray));
	}

	public function addPagination($from,$size) {
		$from = ($from*$size);
		$this->pagination = array(self::FROM=>$from,self::SIZE=>$size);
	}

	public function addSort($sortCriterias) {
		$params = array();
		foreach($sortCriterias as $key => $direction){
			if(strtolower($direction) != strtolower(Direction::ASC) && strtolower($direction) != strtolower(Direction::DESC)) $direction = strtolower(Direction::ASC);
			$params[] = array($key=>array(self::ORDER=>strtolower($direction)));
		}
		$this->sort = array(self::SORT=>$params);
	}

	private function getOrderByValuesArray(){
		foreach(VehicleOrderEnum::getEnumerators() as $vehicleOrder){
			$this->orderByValuesArray[] = $vehicleOrder->getValue();
		}
	}

	/**
	 * @author Livan Frometa <lfrometa@latamautos.com>
	 */
	private function addQuerySite($queryArray) {
		$site = $this->getPageRequest()->getSite();
		$queryMatchArray = array ();
		$queryMatchArray[ElasticsearchKeyEnum::MATCH][self::SITE] = $site;

		if (isset($queryArray[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::MATCH])) {
			$queryArray = $this->transformMatchToBoolMustMatch($queryArray);
			return $this->addQuerySite($queryArray);
		}

		if (isset($queryArray[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::FUNCTION_SCORE])) {
			$queryArray[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::FUNCTION_SCORE] = $this->addQuerySite($queryArray[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::FUNCTION_SCORE]);
			return $queryArray;
		}

		$queryArray[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::BOOL][ElasticsearchKeyEnum::MUST][] = $queryMatchArray;
		return $queryArray;
	}

	private function addQuerySource($source, $queryArray) {
		$queryMatchSource = array();
		$queryMatchSource[ElasticsearchKeyEnum::MATCH][self::SOURCE] = $source;
		$queryArray[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::BOOL][ElasticsearchKeyEnum::MUST][] = $queryMatchSource;
		return $queryArray;
	}

	private function transformMatchToBoolMustMatch($queryMatchAll){
		$queryMatchAll[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::BOOL][ElasticsearchKeyEnum::MUST][] = $queryMatchAll[ElasticsearchKeyEnum::QUERY];
		unset($queryMatchAll[ElasticsearchKeyEnum::QUERY][ElasticsearchKeyEnum::MATCH]);
		return $queryMatchAll;
	}

	public function builtSortCriterias($sortArray){
		$sortParams = array();
		if(!$sortArray) return array();
		foreach($sortArray as $sort){
			$property = $sort->getProperty();
			if(!in_array($property, $this->orderByValuesArray)) $property = VehicleOrderEnum::RELEVANCE;
			if($property == VehicleOrderEnum::RELEVANCE) continue;
			$key = self::ATTRIBUTES .$property. self::VALUE;
			$sortParams[$key] = $sort->getDirection()->getValue();
		}
		return $sortParams;
	}

	public function setClient($client) {
		$this->client = $client;
	}

	public function searchDocumentByMultipleQueries($mquery) {
		$params = array(self::BODY=>$mquery);
		$response = $this->client->msearch($params);
		return $this->convertToDomainModel($response);
	}

	protected function setType($type) {
		$this->type = $type;
	}

	protected function setIndex($index) {
		$this->index = $index;
	}

	protected function getIndex() {
		return $this->index;
	}

	protected function getType() {
		return $this->type;
	}

	public function getTotalPages() {
		return $this->totalPages;
	}

	public function setTotalElements($totalElements) {
		$this->totalElements = $totalElements;
	}

	public function getTotalElements() {
		return $this->totalElements;
	}

}
