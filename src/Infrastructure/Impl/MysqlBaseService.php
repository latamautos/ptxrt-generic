<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Infrastructure\Impl;

use Illuminate\Support\Facades\App;
use Latamautos\Ptxrt\Generic\Infrastructure\Contract\IMonoBase;
use Latamautos\Ptxrt\Utils\Impl\BaseService;

class MysqlBaseService extends BaseService {

	const MANAGER_REGISTER = 'Doctrine\Common\Persistence\ManagerRegistry';
	const DEFAULT_CONNECTION_MONOBASE = 'default';

	public function getManager(){
		if($this instanceof IMonoBase){
			$connectionName = env('DEFAULT_CONNECTION_MONOBASE', self::DEFAULT_CONNECTION_MONOBASE);
		}else{
			$site = $this->getPageRequest()->getSite();
			$connectionName = str_replace('.', '_', $site);
		}
		$managerRegistry = App::make(MysqlBaseService::MANAGER_REGISTER);
		return $managerRegistry->getManager($connectionName);
	}
}