<?php
/**
 * Created by PhpStorm.
 * User: mono
 * Date: 4/2/16
 * Time: 4:49 PM
 */

namespace Latamautos\Ptxrt\Generic\Infrastructure\Impl;

use App\Jobs\CreateSearchTracker;
use Aws\Firehose\FirehoseClient;
use Illuminate\Foundation\Bus\DispatchesJobs;

class FirehoseRepository
{
    const KINESIS_CLIENT_PROFILE = 'kinesis';
    const KINESIS_CLIENT_REGION = 'us-east-1';
    const LATEST = 'latest';
    const PROFILE = 'profile';
    const REGION = 'region';
    const VERSION = 'version';
    const SEPARATOR = "|";
    const NEW_LINE = "\n";
    const DELIVERY_STREAM_NAME = 'DeliveryStreamName';
    const RECORD = 'Record';
    const DATA = 'Data';
    const RECORD_ID = 'RecordId';

    protected $client;

    use DispatchesJobs;

    /**
     * FirehoseRepository constructor.
     */
    public function __construct()
    {
        $this->client = new FirehoseClient([
            self::PROFILE => self::KINESIS_CLIENT_PROFILE,
            self::REGION => self::KINESIS_CLIENT_REGION,
            self::VERSION => self::LATEST
        ]);
    }

    public function createRecord($deliveryStreamName, $redshiftTable, $recordData)
    {
        try {
            $this->putRecord($deliveryStreamName, $redshiftTable, $recordData);
        } catch (\Exception $e) {
            $this->retryPutRecord($deliveryStreamName, $redshiftTable, $recordData);
        }
    }

    public function putRecord($deliveryStreamName, $redshiftTable, $recordData)
    {
        $this->client->putRecord(array(
            self::DELIVERY_STREAM_NAME => $deliveryStreamName,
            self::RECORD => [
                self::DATA => $this->buildData($recordData)
            ],
            self::RECORD_ID => $redshiftTable,
        ));
    }

    /**
     * @param $recordData
     * @return string
     */
    public function buildData($recordData)
    {
        $data = "";
        $lastElement = end($recordData);
        foreach ($recordData as $field) {
            if ($field == $lastElement) {
                $data = $data . $field . self::NEW_LINE;
                return $data;
            }
            $data = $data . $field . self::SEPARATOR;
        }
    }

    /**
     * @param $deliveryStreamName
     * @param $redshiftTable
     * @param $recordData
     * @internal param $searchTracker
     */
    public function retryPutRecord($deliveryStreamName, $redshiftTable, $recordData)
    {
        $job = (new CreateSearchTracker($deliveryStreamName, $redshiftTable, $recordData));
        $this->dispatch($job);
    }

    public function setClient($client)
    {
        $this->client = $client;
    }
}