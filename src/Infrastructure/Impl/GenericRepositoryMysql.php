<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Infrastructure\Impl;

use Doctrine\ORM\Query;
use Illuminate\Support\Facades\App;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Generic\Domain\Contract\IGenericRepository;
use Latamautos\Ptxrt\Generic\Infrastructure\Exception\NotFoundEntityException;
use Latamautos\Ptxrt\Generic\Infrastructure\Exception\NotSavedEntityException;
use Latamautos\Ptxrt\Generic\Infrastructure\Exception\NotDeletedEntityException;

class GenericRepositoryMysql extends MysqlBaseService implements IGenericRepository  {

	protected $em;
	protected $domainModel;
	protected $connectionBySite;

	const TABLE_ALIAS = 'a';

	function __construct($domainModel) {
		$this->em = $this->getManager();
		$this->domainModel = $domainModel;

	}

	public function create($model) {
		try {
			$this->em->persist($model);
			$this->em->flush();
			return $model;
		} catch (\Exception $e) {
			\Log::error($e->getMessage());
			throw new NotSavedEntityException();
		}
	}

	public function update($model) {
		try {
			$this->em->merge($model);
			$this->em->flush();
			return true;
		} catch (\Exception $e) {
			\Log::error($e->getTraceAsString());
			throw new NotSavedEntityException();
		}
	}

	public function delete($model) {
		try {
			$this->em->remove($model);
			$this->em->flush();
			return true;
		} catch (\Exception $e) {
			\Log::error($e->getTraceAsString());
			throw new NotDeletedEntityException();
		}
	}

	public function findById($id) {
		try {
			$entity = $this->em->find(get_class($this->domainModel), $id);
			if (empty($entity)) {
				throw new NotFoundEntityException();
			}
			return $entity;
		} catch (\Exception $e) {
			\Log::error($e->getMessage());
			throw new NotFoundEntityException();
		}
	}

	public function getResultByQuery($queryBuilder){
		return $queryBuilder->getQuery()->getResult();
	}

	public function getResultByQueryAndSite($queryBuilder,$tableAlias = self::TABLE_ALIAS){
		$site = $this->getPageRequest()->getSite();
		$queryBuilder = $queryBuilder->andWhere($tableAlias.'.site = :site')->setParameter(':site', $site);
		return $queryBuilder->getQuery()->getResult();
	}

	public function getEntityManager(){
		return $this->em;
	}

}