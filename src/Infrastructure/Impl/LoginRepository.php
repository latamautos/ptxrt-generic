<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Infrastructure\Impl;

use Latamautos\Ptxrt\Generic\Infrastructure\Contract\ILoginRepository;
use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;
use Latamautos\Ptxrt\Utils\Impl\SocketRequestUtil;

/**
 * @property  sessionImpl
 */
class LoginRepository extends BaseService implements ILoginRepository
{

    const EMAIL_ARRAY_KEY = "email";
    const PASSWORD_ARRAY_KEY = "password";

    const USER_ARRAY_KEY = "user";
    const FACEBOOK_ID_ARRAY_KEY = "facebookId";
    const FIRST_NAME_ARRAY_KEY = "firstName";
    const LAST_NAME_ARRAY_KEY = "lastName";

    const REMOTE_LOGIN_ENDPOINT = "/ptx/api/v2/user/signInOnMobile";

    const REMOTE_LOGIN_FACEBOOK_ENDPOINT = "/ptx/api/v2/user/signInFacebook";

    private $sessionImpl;

    const METHOD_POST = "POST";

    const TIMEOUT = 2;

    function __construct(SessionImpl $sessionImpl)
    {
        $this->sessionImpl = $sessionImpl;
    }

    public function callRemoteLogin($user, $password)
    {
        $requestData = array(
            self::EMAIL_ARRAY_KEY => $user,
            self::PASSWORD_ARRAY_KEY => $password
        );
        return SocketRequestUtil::request($this->getRemoteLoginEndpoint(), $requestData, self::METHOD_POST, CharacterEnum::CHAR_EMPTY, self::TIMEOUT, true);
    }

    public function callRemoteLoginFacebook($facebookId, $email, $firstName, $lastName)
    {
        $requestData = array(
            self::USER_ARRAY_KEY => $email,
            self::FACEBOOK_ID_ARRAY_KEY => $facebookId,
            self::FIRST_NAME_ARRAY_KEY => $firstName,
            self::LAST_NAME_ARRAY_KEY => $lastName
        );
        return SocketRequestUtil::request($this->getRemoteLoginFacebookEndpoint(), $requestData, self::METHOD_POST, CharacterEnum::CHAR_EMPTY, self::TIMEOUT, true);
    }

    private function getRemoteLoginEndpoint()
    {
        return $this->getRemoteServer() . self::REMOTE_LOGIN_ENDPOINT;
    }

    private function getRemoteLoginFacebookEndpoint()
    {
        return $this->getRemoteServer() . self::REMOTE_LOGIN_FACEBOOK_ENDPOINT;
    }
}