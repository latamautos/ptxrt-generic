<?php

namespace Latamautos\Ptxrt\Generic\Infrastructure\Contract;

use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;
use Latamautos\Ptxrt\Utils\Impl\SiteConfigHelper;
use Latamautos\Ptxrt\Utils\Impl\SocketRequestUtil;

interface ILoginRepository
{

    public function callRemoteLogin($user, $password);

    public function callRemoteLoginFacebook($facebookId, $email, $firstName, $lastName);

}