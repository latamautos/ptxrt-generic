<?php

namespace Latamautos\Ptxrt\Generic\Infrastructure\Contract;

use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use Latamautos\Ptxrt\Utils\Impl\SessionImpl;
use Latamautos\Ptxrt\Utils\Impl\SiteConfigHelper;
use Latamautos\Ptxrt\Utils\Impl\SocketRequestUtil;

interface IUserRepository
{
    public function callRemoteSignUp($email, $password, $name, $cellphone = "0", $subscribeNewsletter = 1);

    public function callRemoteRecoverPassword($email);
}