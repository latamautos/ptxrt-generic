<?php
/**
 * @author Harold Portocarrero <hportocarrero@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Infrastructure\Exception;

class NotFoundDocumentException extends \Exception {

	function __construct() {
		parent::__construct("Not Found Document");
	}

}