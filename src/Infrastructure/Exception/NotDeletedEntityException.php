<?php
/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\Infrastructure\Exception;

class NotDeletedEntityException extends \Exception {

	function __construct() {
		parent::__construct("Not deleted Entity");
	}

}