<?php
/**
 * Created by PhpStorm.
 * User: denMac
 * Date: 24/9/15
 * Time: 12:34
 */

$navLinks = array('Clasificados'    => array('/', 'vehiculo'),
                'Catálogo Nuevos'   => array('/catalogo-nuevos'),
                'Opiniones'         => array('/opiniones'),
                'Avaluador'         => array('#'),
                'Comparador'        => array('#'),
                'Guías y Consejos'  => array('#'));
?>
<!DOCTYPE html>
<html lang="en" ng-app="ptx" class="latam-theme">
<head>
        <meta charset="utf-8"/>
        <title>@yield('title', Lang::get('PATIOTuerca'))</title>
        <meta name='description' content='@yield('description', Lang::get('labels.title'))'>
{{--@if(ConfigPTX::$development)--}}
        {{--<meta name="robots" content="noindex, nofollow">--}}
{{--@endif--}}
        <meta name="revisit-after" content="1 days">
        <meta name="robots" content="NOODP">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="dns-prefetch" href="//s3.amazonaws.com">
        <link rel="dns-prefetch" href="//google-analytics.com">
        <link rel="dns-prefetch" href="//fotos_nginx.patiotuerca.com">
        <link rel="dns-prefetch" href="//googletagservices.com">
        <link rel="dns-prefetch" href="//tpc.googlesyndication.com">
        <link rel="dns-prefetch" href="//partner.googleadservices.com">
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com">
        <link rel="dns-prefetch" href="//facebook.com">
        <link rel="dns-prefetch" href="//track.hubspot.com">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}"/>
        @include('Generic.Presentation.assets.layout.partials._headScripts')
        @section('headerAssets')
        @show
</head>
<body>
@include('Generic.Presentation.assets.layout.partials._headerScripts')
@include('Generic.Presentation.assets.layout.partials._aside')
<div class="over-footer">
        <header id="public-layout">
                @include('Generic.Presentation.assets.layout.partials._header')
        </header>
        <main id="content">
                @section('content')
                @show
        </main>
        <footer>
                @include('Generic.Presentation.assets.layout.partials._footer')
        </footer>
</div>
@include('Generic.Presentation.assets.layout.partials._backFooter')
@section('outContent')
@show
@include('Generic.Presentation.assets.layout.partials._loginModal')
@include('Generic.Presentation.assets.layout.partials._footerScripts')
@section('footerAssets')
@show

</body>
</html>