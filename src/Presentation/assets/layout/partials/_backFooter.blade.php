<div class="back-footer white-text">
    <section class="container p-t">
        <div class="row p-t p-b center-align m-t-lg m-b-lg">
            <div class="col s4"><a href="" class="btn btn-svg btn-large waves-effect waves-light btn-border-light"><span class="hide-on-small-and-down">hazte fan</span><img src="{{asset('assets/materialTheme/svg/facebook.svg')}}" class="facebook no-margin-xs m-r x-small left svg white-svg"/></a></div>
            <div class="col s4"><a href="" class="btn btn-svg btn-large waves-effect waves-light btn-border-light"><span class="hide-on-small-and-down">síguenos</span><img src="{{asset('assets/materialTheme/svg/twitter.svg')}}" class="facebook no-margin-xs m-r x-small left svg white-svg"/></a></div>
            <div class="col s4"><a href="" class="btn btn-svg btn-large waves-effect waves-light btn-border-light"><span class="hide-on-small-and-down">suscríbete</span><img src="{{asset('assets/materialTheme/svg/youtube.svg')}}" class="facebook no-margin-xs m-r x-small left svg white-svg"/></a></div>
        </div>
        <div class="row copy-ft p-t p-b m-b-none">
            <div class="col m6 s12 center-align-xs">
                COPYRIGHT © 2004 - {{date('Y')}} | <a href="" class="latam-primary-text">PATIOTuerca.com</a>
            </div>
            <div class="col m6 s12 right-align m-t-size-xs center-align-xs">
                Un producto de: <img src="{{asset('assets/brands/latamautos_w.svg')}}" class="svg"/>
            </div>
        </div>
    </section>
</div>