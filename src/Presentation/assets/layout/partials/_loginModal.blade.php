<div id="loginModal" class="modal modal-login">
	<div class="modal-content">
		<div class="gif-area"></div>
		<div class="button-area" id="loginFormsList">
			<div class="login block-form active">
				<a href="" class="btn blue darken-3 btn-justify">ingresar con facebook</a>
				<a href="" class="btn latam-primary btn-justify m-t waves-effect waves-light" id="showLogin">ingresar con tu cuenta PATIOTuerca</a>
				<p class="center-align">¿no tienes cuenta? <a href="" class="latam-info-text" id="createAccount">crear cuenta</a></p>
			</div>
			<div class="login-form block-form">
				<form class="col s12 p-t" name="login">
					<div class="row">
						<div class="input-field col s12">
							<input id="email" type="email" class="validate">
							<label for="email">Email</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="password" type="password" class="validate">
							<label for="password">Password</label>
						</div>
						<div class="right-align col s12 size-small"><a class="latam-secondary-text text-lighten-2" href="" id="showPass">¿Olvidaste tu contraseña?</a></div>
					</div>
					<div class="row">
						<div class="col s6">
							<a href="#!" class="btn btn-flat waves-effect waves-light btn-justify m-t back-login">Cancelar</a>
						</div>
						<div class="col s6">
							<a href="" class="btn latam-primary btn-justify waves-effect waves-light m-t">iniciar seción</a>
						</div>
					</div>
				</form>
			</div>
			<div class="pass-form block-form">
				<form class="col s12 p-t" name="password">
					<div class="row">
						<div class="input-field col s12">
							<input id="email" type="email" class="validate">
							<label for="email">Email</label>
						</div>
					</div>
					<div class="row">
						<div class="col s6">
							<a href="#!" class="btn btn-flat waves-effect waves-light btn-justify m-t back-login">Cancelar</a>
						</div>
						<div class="col s6">
							<a href="" class="btn latam-primary btn-justify waves-effect waves-light m-t">recuperar contraseña</a>
						</div>
					</div>
				</form>
			</div>
			<div class="register-form block-form">
				<form class="col s12 p-t" name="register">
					<div class="row">
						<div class="input-field col s12">
							<input id="name" type="text" class="validate">
							<label for="name">Nombre y Apellido</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="email" type="email" class="validate">
							<label for="email">Email</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="phone" type="number" class="validate">
							<label for="phone">Teléfono</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<input id="password" type="password" class="validate">
							<label for="password">Password</label>
						</div>
					</div>
					<div class="row">
						<div class="col s6">
							<a href="#!" class="btn btn-flat waves-effect waves-light btn-justify m-t back-login">Cancelar</a>
						</div>
						<div class="col s6">
							<a href="" class="btn latam-primary btn-justify waves-effect waves-light m-t">Registrarce</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>