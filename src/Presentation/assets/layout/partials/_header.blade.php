<nav class="brand-area">
    <div class="nav-wrapper">
{{--@startTrop--}}
        <a href="/" class="navbar-brand"><img class="svg" src="{{asset('assets/brands/patiotuerca.svg')}}" alt="patioTuerca"/></a>
{{--@endTrop('contents.logo')--}}
        {{--<form>--}}
            {{--<div class="input-field">--}}
                {{--<label class="label truncate" for="icon_prefix">Buscar marca, modelo, característica</label>--}}
                {{--<input id="search" type="search" required>--}}
                {{--<label for="search"><i class="material-icons">search</i></label>--}}
                {{--<i class="material-icons">close</i>--}}
            {{--</div>--}}
        {{--</form>--}}
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="modal-trigger waves-effect" href="#loginModal" id="registerBtn">Crear Cuenta</a></li>
            <li><a class="modal-trigger waves-effect" href="#loginModal">Iniciar Seción</a></li>
            <li class="cta-btn"><a class="waves-effect waves-light latam-primary btn-large"><img src="{{asset('assets/materialTheme/svg/publish.svg')}}" class="svg small m-t-md left m-r-md"/>Publica Gratis</a></li>
        </ul>
    </div>
</nav>
<section class="nav-area hide-on-med-and-down z-depth-1">
    <nav class="sections-nav left m-l">
        <ul id="nav-mobile" class="left hide-on-med-and-down">
            @foreach($navLinks as $key=>$value)
                @foreach($value as $active)
                    @if(Request::url()==url($active))
                        <?php $class="active"; break; ?>
                    @else
                        <?php $ac = 'href='.url($value[0]); $class="waves-effect";?>
                    @endif
                @endforeach
                <li class="{{ isset($class) ? $class : null }}"><a {{isset($ac) ? $ac : null}}>{{$key}}</a></li>
            @endforeach
            <span class="active-sw"></span>
        </ul>
    </nav>
    <nav class="social-nav right m-r">
        <ul id="nav-social" class="hide-on-med-and-down">
            <li><a class="waves-effect" href="#"><img src="{{asset('assets/materialTheme/svg/facebook.svg')}}" class="facebook svg"/><span class="txt-btn">Hazte Fan</span><span class="m-l latam-secondary lighten-2 badge">1</span></a></li>
            <li><a class="waves-effect" href="#"><img src="{{asset('assets/materialTheme/svg/twitter.svg')}}" class="twitter svg"/><span class="txt-btn">Síguenos</span><span class="m-l latam-secondary lighten-2 badge">1</span></a></li>
        </ul>
    </nav>
    <div class="clearfix"></div>
</section>