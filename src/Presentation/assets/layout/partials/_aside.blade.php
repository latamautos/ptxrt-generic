<nav class="hide-on-large-only mobile_nav">
    <ul id="slide-out" class="side-nav">
        <li class="top-links">
            <a href="#" class="waves-effect waves-light latam-primary btn-large white-text">Publica Gratis <img src="{{asset('assets/materialTheme/svg/publish.svg')}}" class="svg medium m-t left m-r-md"/></a>
        </li>
        <li class="top-links">
            <a href="#" class="waves-effect waves-light btn-large btn-flat">Crear Cuenta</a>
        </li>
        <li class="top-links">
            <a class="waves-effect waves-light btn-large btn-flat modal-trigger" href="#loginModal">Iniciar Seción</a>
        </li>
        <hr class="no-margin">

        @foreach($navLinks as $key=>$value)
            @foreach($value as $active)
                @if(Request::url()==url($active))
                    <?php $ac = 'href='.url($value[0]); $class="active"; break; ?>
                @else
                    <?php $class="waves-effect"?>
                @endif
            @endforeach
            <li><a class="{{$class}}" {{isset($ac) ? $ac : null}}>{{$key}}</a></li>
        @endforeach


    </ul>
</nav>
<a href="#" data-activates="slide-out" class="button-collapse hide-on-large-only aside-mobile-btn"><i class="mdi-navigation-menu"></i></a>