<div class="latam-secondary lighten-1 footer_contact white-text">
    <section class="container p-t p-b">
        <div class="row p-t">
            <div class="col s12">
                <ul class="sections light">
                    <li><a href="">Inicio</a></li>
                    <li><a href="">Nuevos</a></li>
                    <li><a href="">Usados</a></li>
                    <li><a href="">Motorbit:Contenidos y novedades</a></li>
                    <li><a href="">Ayuda</a></li>
                </ul>
            </div>
            <div class="col m6 s12 contact-info size-small latam-secondary-text text-lighten-3">
                <p><span class="white-text">Contáctanos al 1700 TUERCA</span> <br>
                    Usar este sitio implica aceptar nuestros <a href="" class="latam-primary-text">Términos y Condiciones</a> de Uso Prohibida su reproducción total o parcial, así como su traducción a cualquier idioma sin autorización escrita de su titular.</p>
            </div>
            <div class="col m6 s12 app-links center-align m-t-size-xs">
                <a class="btn-img waves-effect waves-light btn"><img src="{{asset('assets/materialTheme/images/app_store.png')}}" alt=""></a><a class="btn-img waves-effect waves-light btn"><img src="{{asset('assets/materialTheme/images/google_play.png')}}" alt=""></a>
            </div>
        </div>
        <div class="row m-t center-align latam-brands m-t-lg">
            <div class="col m3 s6"><a href=""><img src="{{asset('assets/brands/patiotuerca.svg')}}" class="svg" alt=""></a></div>
            <div class="col m3 s6"><a href=""><img src="{{asset('assets/brands/todoautos.svg')}}" class="svg" alt=""></a></div>
            <div class="col m3 s6"><a href=""><img src="{{asset('assets/brands/seminuevos.svg')}}" class="svg" alt=""></a></div>
            <div class="col m3 s6"><a href=""><img src="{{asset('assets/brands/autofoco.svg')}}" class="svg" alt=""></a></div>
        </div>
    </section>
</div>