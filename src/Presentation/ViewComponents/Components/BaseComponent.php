<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 12/18/15
 * Time: 4:00 PM
 */

namespace Latamautos\Ptxrt\Generic\Presentation\ViewComponents\Components;


interface BaseComponent
{

    function getDefaultPath();

    function make();
}