<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 12/18/15
 * Time: 3:59 PM
 */

namespace Latamautos\Ptxrt\Generic\Presentation\ViewComponents\Components;

use Latamautos\Ptxrt\Generic\Presentation\Util\BaseLink;
use Latamautos\Ptxrt\Search\Presentation\Enum\CharacterEnum;
use MkDoctrineSpringData\Pagination\PageImpl;

class Pagination implements BaseComponent
{

    const SHOW_FIRST = "SHOW_FIRST";
    const SHOW_LAST = "SHOW_LAST";
    const SIBLINGS_COUNT = "SIBLINGS_COUNT";
    const LEFT_ARROW = "<";
    const RIGHT_ARROW = ">";
    const SIBLINGS_DEFAULT_COUNT = 2;
    const BLADE_PATH = "";
    const PAGE = "page";
    const SIZE = "size";
    const DATA = "data";
    const REQUEST_URI = "REQUEST_URI";
    const QUERY_STRING = 'QUERY_STRING';
    const REGEX = '/(?:^|(?<=&))[^=[]+/';
    const HEX_2_BIN = 'hex2bin';

    private $url;
    private $queryString;
    private $pageImpl;
    private $params;
    private $basicLinks;
    private $siblingsCount;
    private $queryStringParams;

    public function __construct(PageImpl $pageImpl, array $params = [])
    {
        $this->url = isset($_SERVER[self::REQUEST_URI]) ? strtok($_SERVER[self::REQUEST_URI], CharacterEnum::QUESTION) : CharacterEnum::CHAR_EMPTY;
        $queryString = isset($_SERVER[self::REQUEST_URI]) ? urldecode($_SERVER[self::QUERY_STRING]) : CharacterEnum::CHAR_EMPTY;
        $this->queryString = $queryString;
        $this->pageImpl = $pageImpl;
        $this->params = $params;
        $this->basicLinks = [];
        $this->siblingsCount = self::SIBLINGS_DEFAULT_COUNT;
        if ($this->hasTrueParamByKey(self::SIBLINGS_COUNT)) $this->siblingsCount = intval($params[self::SIBLINGS_COUNT]);
        $this->queryStringParams = $this->parse_qs($queryString);
//		dd($this->queryStringParams);
    }

    function parse_qs($data)
    {
        $pieces = explode("&", $data);
        $params = [];
        foreach ($pieces as $piece) {
            $values = explode("=", $piece);
            if (count($values) < 2) break;
            $values[0] = substr($values[0], strpos(urldecode($values[0]), "["));

            if (!isset($params[$values[0]])) {
                $params[$values[0]] = [];

            }
            $params[$values[0]][] = $values[1];
        }
        return $params;

    }

    function getPath($page)
    {
        $size = $this->pageImpl->getSize();
        if (empty($this->queryStringParams)) $this->queryStringParams = [];
        $this->queryStringParams[self::PAGE] = $page;
        $this->queryStringParams[self::SIZE] = $size;
        return $this->url . CharacterEnum::QUESTION . str_replace(" ", "+", urldecode($this->httpBuildQuery($this->queryStringParams)));
    }

    private function httpBuildQuery($queryStringParams)
    {
        $queryString = "";
        $removeAmp = false;
        foreach ($queryStringParams as $paramKey => $values) {
            if (is_array($values)) {
                foreach ($values as $value) {
                    $removeAmp = true;
                    $queryString .= $paramKey . "=" . $value . "&";
                }
            } else {
                $removeAmp = true;
                $queryString .= $paramKey . "=" . $values . "&";
            }

        }
        return $removeAmp ? substr($queryString, 0, strlen($queryString) - 1) : $queryString;
    }

    function make()
    {
        if ($this->pageImpl->getTotalElements() <= 0) return view($this->getDefaultPath(), [self::DATA => []]);
        $this->addArrows();
        $this->addFirstLink();
        $this->addLastLink();
        $this->addActualPage();
        $this->addSiblingLinks();
        $this->sort();
        return view($this->getDefaultPath(), [self::DATA => $this->getBasicLinks()]);
    }

    protected function addArrows()
    {
        $this->addOrReplace(new BaseLink($this->hasNoPreviousHref() ? null : $this->getPath($this->getPreviousPageNumber()), self::LEFT_ARROW));
        $this->addOrReplace(new BaseLink($this->hasNoNextHref() ? null : $this->getPath($this->getNextPageNumber()), self::RIGHT_ARROW));
    }

    private function addOrReplace(BaseLink $baseLink)
    {
        $hasItem = false;
        array_walk($this->basicLinks, $f = function (BaseLink &$value, $key) use ($baseLink, &$hasItem) {
            $hasItem = $baseLink->getValue() == $value->getValue();
        });
        if (!$hasItem) $this->addItemToBaseLinks($baseLink);
    }

    private function addItemToBaseLinks(BaseLink $baseLink)
    {
        $this->basicLinks[] = $baseLink;
    }

    public function getBasicLinks()
    {
        return $this->basicLinks;
    }

    private function addFirstLink()
    {
        if ($this->hasTrueParamByKey(self::SHOW_FIRST)) {
            $this->addOrReplace(new BaseLink($this->pageImpl->isFirst() ? null : $this->getPath(0), 1));
        }
    }

    private function addLastLink()
    {
        if ($this->hasTrueParamByKey(self::SHOW_LAST)) {
            $this->addOrReplace(new BaseLink($this->pageImpl->isLast() ? null : $this->getPath($this->pageImpl->getTotalPages()), $this->pageImpl->getTotalPages()));
        }
    }

    private function hasTrueParamByKey($key)
    {
        return isset($this->params[$key]) && $this->params[$key] == true;
    }

    private function sort()
    {
        usort($this->basicLinks, function (BaseLink $a, BaseLink $b) {
            if ($a->getValue() == self::LEFT_ARROW || $b->getValue() == self::RIGHT_ARROW) return -1000;
            if ($a->getValue() == self::RIGHT_ARROW || $b->getValue() == self::LEFT_ARROW) return 1000;
            return intval($a->getValue()) - intval($b->getValue());
        });
    }


    private function addActualPage()
    {
        if ($this->pageImpl->getTotalElements() < 1) return;
        $this->addOrReplace(new BaseLink(null, $this->pageImpl->getNumber() + 1, true));
    }

    private function addSiblingLinks()
    {
        for ($siblings = 1; $siblings <= $this->siblingsCount; $siblings++) {
            $previousSiblingValue = $this->pageImpl->getNumber() - $siblings;
            $nextSiblingValue = $this->pageImpl->getNumber() + $siblings;
            if ($previousSiblingValue >= 0)
                $this->addOrReplace(new BaseLink($this->getPath($previousSiblingValue), $previousSiblingValue + 1));
            if ($nextSiblingValue < $this->pageImpl->getTotalPages())
                $this->addOrReplace(new BaseLink($this->getPath($nextSiblingValue), $nextSiblingValue + 1));
        }
    }


    public function setSiblingsCount($siblingsCount)
    {
        if ($siblingsCount == 0)
            $siblingsCount = self::SIBLINGS_DEFAULT_COUNT;
        $this->siblingsCount = $siblingsCount;
    }


    function getDefaultPath()
    {
        return "Generic.Presentation.ViewComponents.Views.pagination";
    }


    protected function hasNoPreviousHref()
    {
        return $this->pageImpl->getTotalPages() == 1 || $this->pageImpl->isFirst();
    }

    protected function hasNoNextHref()
    {
        return $this->pageImpl->getTotalPages() == 1 || !$this->pageImpl->hasNext();
    }

    protected function getNextPageNumber()
    {
        return $this->pageImpl->nextPageable()->getPageNumber();
    }


    protected function getPreviousPageNumber()
    {
        return $this->pageImpl->previousPageable()->getPageNumber();
    }
}