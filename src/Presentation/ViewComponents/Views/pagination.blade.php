<ul class="pagination center-align">
 @foreach($data as $item)
  <li class="waves-effect {{isset($item->getHref()) ? 'active' : null}}"><a {{isset($item->getHref()) ? 'href='.$item->getHref() : null}}>{{$item->getValue()}}</a></li>
 @endforeach
</ul>