<?php

namespace Latamautos\Ptxrt\Generic\DistributedServices\Interceptors;

use Closure;
use Latamautos\Ptxrt\Utils\Impl\RequestManager;
use Latamautos\Ptxrt\Utils\Impl\KeyValuesPairConverter;

class PageRequestMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
				$pageRequestInterceptor = new PageRequestInterceptor(new RequestManager(), new KeyValuesPairConverter());
				$pageRequestInterceptor->persistPageRequest($request->server->get('QUERY_STRING'));
        return $next($request);
    }
}
