<?php
/**
 * Created by PhpStorm.
 * User: haroldportocarrero
 * Date: 18/11/15
 * Time: 16:58
 */

namespace Latamautos\Ptxrt\Generic\DistributedServices\Interceptors;

use Latamautos\Ptxrt\Search\Domain\Enum\VehicleOrderEnum;
use MkDoctrineSpringData\Pagination\PageRequest;
use Latamautos\Ptxrt\Utils\Impl\KeyValuesPairConverter;
use MkDoctrineSpringData\Pagination\Sort;
use MkDoctrineSpringData\Pagination\Sorting\Direction;
use Latamautos\Ptxrt\Utils\Impl\RequestManager;
use Latamautos\Ptxrt\Utils\Enum\CatalogPrefixEnum;

class PageRequestInterceptor {

	const SIZE = "size";
	const PAGE = "page";
	const DESC = "desc";
	const TOTAL_ITEMS = "total_items";
	const TOTAL_PAGES = "total_pages";
	const ASC = "asc";
	const DEFAULT_PAGE = 0;
	const DELIMITER = ",";
	const PAGE_REQUEST_KEY = 'pageRequest';
	const DEFAULT_SIZE = 'DEFAULT_SIZE';
	const SORT_CRITERIAS = 'sortCriterias';

	private $orderByEquivalences = array ("ORDER_BY", "order_by", "orderBy", "order");
	private $apiSortValuesMapping = array(CatalogPrefixEnum::MILEAGE=>CatalogPrefixEnum::MILEAGE,CatalogPrefixEnum::PUBLISHED_AT=>CatalogPrefixEnum::PUBLISHED,CatalogPrefixEnum::HIGHLIGHTED=>CatalogPrefixEnum::RELEVANCE, CatalogPrefixEnum::MILEAGEONLY=>CatalogPrefixEnum::MILEAGE, CatalogPrefixEnum::PUBLISHED=>CatalogPrefixEnum::PUBLISHED_AT);
	private $size;
	private $page = self::DEFAULT_PAGE;
	private $sortCriterias = array ();
	private $pageRequest;
	private $requestManager;
	private $keyValuePairDtoConverter;

	function __construct(RequestManager $requestManager, KeyValuesPairConverter $keyValuePairDtoConverter) {
		$this->requestManager = $requestManager;
		$this->keyValuePairDtoConverter = $keyValuePairDtoConverter;
	}

	public function persistPageRequest($queryString) {
		$this->iterateQueryString($queryString);
		$this->persistPageRequestOnList();
	}

	private function iterateQueryString($queryString) {
		$this->size = env(self::DEFAULT_SIZE);
		if ($queryString == null) return null;
		$arrayQueryString = $this->keyValuePairDtoConverter->convertToArray($queryString);
		foreach ($arrayQueryString as $dto) {
			$value = $dto->getValues();
			$key = $dto->getKey();
			$this->setParamsPageRequest($key, $value);
		}
	}

	public function convertOrderParamsToSort($arrayOrderByParams) {
		$sort = null;
		foreach ($arrayOrderByParams as $index => $param) {
			$orderParams = explode(self::DELIMITER, $param);
			if ($this->hasNoOrderParams($orderParams)) continue;
			$property = strtolower($orderParams[0]);
			$direction = $this->getEnumDirectionForParamOrder(isset($orderParams[1]) ? $orderParams[1] : $this->setDirectionIfAPIRequest());
			if (empty($property)) continue;
			if ($index == 0) {
				$sort = new Sort($direction, $property);
			} else {
				$sort->andSort(new Sort($direction, $property));
			}
		}
		if(!$sort) $sort = new Sort(Direction::ASC(), VehicleOrderEnum::RELEVANCE);
		return $sort;
	}

	private function getEnumDirectionForParamOrder($direction) {
		$direction = strtolower($direction);
		return ($direction == self::ASC) ? Direction::ASC() : (($direction == self::DESC) ? Direction::DESC() : null);
	}

	public function persistPageRequestOnList() {
		if ($this->hasSortCriteria()) {
			$this->pageRequest = new PageRequest($this->page, $this->size, $this->sortCriterias);
		} else {
			$this->pageRequest = new PageRequest($this->page, $this->size);
		}
		$this->requestManager->persistDataOnList($this->pageRequest, self::PAGE_REQUEST_KEY);
	}

	public function getPageRequest(){
		return $this->pageRequest;
	}

	public function setParamsPageRequest($key, $value) {
		if ($this->hasKeyOrderByArray($key, $this->orderByEquivalences)) $this->sortCriterias = $this->convertOrderParamsToSort($this->mapSearchAPISortValuesToSearchSortValues($value));
		$key = strtolower($key);
		if ($key == self::PAGE) $this->page = $value[0];
		if ($key == self::SIZE)  $this->size = $value[0];
	}

	public function getParamsPageRequest() {
		return array (self::PAGE => $this->page, self::SIZE => $this->size, self::SORT_CRITERIAS => $this->sortCriterias);
	}

	private function hasNoOrderParams($orderParams) {
		return sizeof($orderParams) <= 0;
	}

	private function hasSortCriteria() {
		return sizeof($this->sortCriterias) > 0;
	}

	private function hasKeyOrderByArray($key, $orderByArray){
		return in_array($key, $orderByArray);
	}

	private function mapSearchAPISortValuesToSearchSortValues($apiSortValuesArray){
		$resultApiSortValuesArray = array();
		foreach($apiSortValuesArray as $key => $value) $resultApiSortValuesArray[$key] = str_replace(array_keys($this->apiSortValuesMapping), array_values($this->apiSortValuesMapping), strtolower($value));
		return $resultApiSortValuesArray;
	}

	private function setDirectionIfAPIRequest(){
		return (isset($_REQUEST['desc']) ? Direction::DESC() : null);
	}
}