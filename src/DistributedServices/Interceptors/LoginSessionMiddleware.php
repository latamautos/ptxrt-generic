<?php

namespace Latamautos\Ptxrt\Generic\DistributedServices\Interceptors;

use Closure;
use Latamautos\Ptxrt\Utils\Impl\LoginUtil;

class LoginSessionMiddleware
{

    const USER_EMAIL_KEY = "userEmail";
    const TOKEN_KEY = "token";
    const TIMESTAMP_KEY = "ts";

    /**
     * Handle an incoming request. Check if it has token, userEmail and ts and if it does, set session userEmail
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->input(self::TOKEN_KEY);
        $userEmail = $request->input(self::USER_EMAIL_KEY);
        $ts = $request->input(self::TIMESTAMP_KEY);
        if($this->validToken($token,$userEmail, $ts)){
            //TODO llenar con lo que se necesite
            $request->session()->put(self::USER_EMAIL_KEY, $userEmail);
        }
        return $next($request);
    }

    private function validToken($token, $userEmail, $ts){
        if($token != null && $userEmail != null && $ts != null){
            $loginUtil = new LoginUtil($userEmail, $ts);
            if($loginUtil->validHash($token)){
                return true;
            }
        }
        return false;
    }
}
