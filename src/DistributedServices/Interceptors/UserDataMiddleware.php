<?php

namespace Latamautos\Ptxrt\Generic\DistributedServices\Interceptors;

use Closure;
use Latamautos\Ptxrt\Utils\Impl\RequestManager;
use Latamautos\Ptxrt\Utils\Impl\KeyValuesPairConverter;

class UserDataMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userDataInterceptor = new UserDataInterceptor(new RequestManager(),$request);
        $userDataInterceptor->setAllDataInRequest();
        return $next($request);
    }
}
