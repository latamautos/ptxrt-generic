<?php

/**
 * @author Livan Frometa <lfrometa@latamautos.com>
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\DistributedServices\Controllers;

use Laravel\Lumen\Routing\Controller;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Latamautos\Ptxrt\Generic\DistributedServices\Interceptors\PageRequestInterceptor;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use MkDoctrineSpringData\Pagination\PageImpl;

class BaseRestController extends Controller
{
    const STATUS_LABEL = "status";
    const MESSAGES_LABEL = "messages";
    const DATA_LABEL = "data";
    const NOTIFICATIONS_LABEL = "notifications";

    protected $baseService;
    protected $thrownException;

    public function __construct()
    {
        $this->baseService = new BaseService();
    }

    public function callAction($method, $parameters)
    {
        $response = array();
        $this->baseService = $this->getBaseServiceInstance();

        $this->baseService->getPageRequest();
        try {
            $resultFromMethod = call_user_func_array(array($this, $method), $parameters);
            if ($resultFromMethod instanceof PageImpl) return $this->createPagedResult($resultFromMethod);
            return $this->createResult($resultFromMethod);
        } catch (\Exception $e) {
            $response[self::DATA_LABEL] = array();
            $this->thrownException = $e;
        } finally {
            $this->setStatus($response);
            $this->setNotifications($response);
        }
        return $this->serializeResponse($response);
    }

    public function response($json)
    {
        $response = array();
        $this->baseService = $this->getBaseServiceInstance();

        $this->baseService->getPageRequest();
        try {
            $resultFromMethod = $json;
            if ($resultFromMethod instanceof PageImpl) return $this->createPagedResult($resultFromMethod);
            return $this->createResult($resultFromMethod);
        } catch (\Exception $e) {
            $response[self::DATA_LABEL] = array();
            $this->thrownException = $e;
        } finally {
            $this->setStatus($response);
            $this->setNotifications($response);
        }
        return $this->serializeResponse($response);
    }

    protected function formatObject($object)
    {
        $context = SerializationContext::create()->enableMaxDepthChecks();
        $context->setSerializeNull(true);
        $serializer = SerializerBuilder::create()->build();

        return $serializer->serialize($object, 'json', $context);
    }

	  protected function setStatus(&$response)
    {
        if ($this->baseService->getNotifications()->hasErrors()) {
            $response[self::STATUS_LABEL] = $this->baseService->getNotifications()->getStatusCode();
            //TODO transformar de notifications a un string y ponerlo en messages_label
            $response[self::MESSAGES_LABEL] = "errors";
        } else if ($this->thrownException) {
            $response[self::STATUS_LABEL] = 500;
            $response[self::MESSAGES_LABEL] = $this->thrownException->getMessage();
        } else {
            $response[self::STATUS_LABEL] = 200;
            $response[self::MESSAGES_LABEL] = null;
        }

    }

	protected function setNotifications(&$response)
    {
        $response[self::NOTIFICATIONS_LABEL] = $this->baseService->getNotifications()->getMessages();
    }

	protected function getBaseServiceInstance()
    {
        if (!$this->baseService) $this->baseService = new BaseService();
        return $this->baseService;
    }

	protected function createPagedResult(PageImpl $resultFromMethod)
    {
        $response = [];
        $response[self::DATA_LABEL] = $resultFromMethod->getContent();
        $this->setStatus($response);
        $this->setNotifications($response);
        $this->setPagination($response, $resultFromMethod);
        return $this->serializeResponse($response);
    }

	protected function createResult($resultFromMethod)
    {
        $response = [];
        $response[self::DATA_LABEL] = $resultFromMethod;
        $this->setStatus($response);
        $this->setNotifications($response);
        return $this->serializeResponse($response);
    }

    /**
     * @param $response
     * @return mixed
     */
    protected function serializeResponse($response)
    {
        $jsonResponse = response()->json($response, $response[self::STATUS_LABEL]);
        return $jsonResponse;
    }

	protected function setPagination(&$response, PageImpl $pageImpl)
    {
        $response[PageRequestInterceptor::PAGE] = $pageImpl->getNumber();
        $response[PageRequestInterceptor::SIZE] = $pageImpl->getSize();
        $response[PageRequestInterceptor::TOTAL_ITEMS] = $pageImpl->getTotalElements();
        $response[PageRequestInterceptor::TOTAL_PAGES] = $pageImpl->getTotalPages();
    }

}
