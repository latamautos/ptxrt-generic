<?php

/**
 * @author Marco Guillén,  Edgar Vintimilla
 * @version 1.0.0
 */

namespace Latamautos\Ptxrt\Generic\DistributedServices\Controllers;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Latamautos\Ptxrt\Generic\DistributedServices\Interceptors\PageRequestInterceptor;
use Latamautos\Ptxrt\Utils\Impl\BaseService;
use MkDoctrineSpringData\Pagination\PageImpl;

class APIBaseRestController extends BaseRestController
{
	public function callAction($method, $parameters){
		$response = array();
		$this->setAPIOrder($response);
		$this->baseService = parent::getBaseServiceInstance();
		$this->baseService->getPageRequest();
		try {
			$resultFromMethod = call_user_func_array(array($this, $method), $parameters);
			if ($resultFromMethod instanceof PageImpl) return $this->createPagedResult($resultFromMethod);
			$this->setAPIOrder($response);
			return $this->createResult($resultFromMethod);
		} catch (\Exception $e) {
			$response[self::DATA_LABEL] = array();
			$this->thrownException = $e;
		} finally {
			parent::setStatus($response);
			parent::setNotifications($response);
		}
		return parent::serializeResponse($response);
	}

	protected function createPagedResult(PageImpl $resultFromMethod)
	{
		$response = [];
		$response[self::DATA_LABEL] = $resultFromMethod->getContent();
		$this->setAPIOrder($response);
		parent::setStatus($response);
		parent::setNotifications($response);
		parent::setPagination($response, $resultFromMethod);
		return parent::serializeResponse($response);
	}

	protected function createResult($resultFromMethod){
		$response = [];
		$response[self::DATA_LABEL] = $resultFromMethod;
		$this->setAPIOrder($response);
		$this->setStatus($response);
		$this->setNotifications($response);
		return parent::serializeResponse($response);
	}


	private function setAPIOrder(&$response){
		$response['ORDER'] = 'ORDER....';
	}


}