<?php
/**
 * Created by PhpStorm.
 * User: xavier
 * Date: 8/31/15
 * Time: 2:02 PM
 */

namespace Latamautos\Ptxrt\Generic\Providers;


use Illuminate\Support\ServiceProvider;

class BindingServiceProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->bind('Latamautos\Ptxrt\Generic\Application\Contract\IFavoriteService', 'Latamautos\Ptxrt\Generic\Application\Impl\FavoriteService');
		$this->app->bind('Latamautos\Ptxrt\Generic\Application\Contract\ILoginService', 'Latamautos\Ptxrt\Generic\Application\Impl\LoginService');
		$this->app->bind('Latamautos\Ptxrt\Generic\Application\Contract\IUserService', 'Latamautos\Ptxrt\Generic\Application\Impl\UserService');
        $this->app->bind('Latamautos\Ptxrt\Generic\Domain\Contract\IFavoriteHandler', 'Latamautos\Ptxrt\Generic\Domain\Impl\FavoriteHandler');
        $this->app->bind('Latamautos\Ptxrt\Generic\Domain\Contract\ILoginHandler', 'Latamautos\Ptxrt\Generic\Domain\Impl\LoginHandler');
        $this->app->bind('Latamautos\Ptxrt\Generic\Domain\Contract\IUserHandler', 'Latamautos\Ptxrt\Generic\Domain\Impl\UserHandler');
        $this->app->bind('Latamautos\Ptxrt\Generic\Infrastructure\Contract\IFavoriteRepository', 'Latamautos\Ptxrt\Generic\Infrastructure\Impl\FavoriteRepository');
        $this->app->bind('Latamautos\Ptxrt\Generic\Infrastructure\Contract\ILoginRepository', 'Latamautos\Ptxrt\Generic\Infrastructure\Impl\LoginRepository');
        $this->app->bind('Latamautos\Ptxrt\Generic\Infrastructure\Contract\IUserRepository', 'Latamautos\Ptxrt\Generic\Infrastructure\Impl\UserRepository');
	}
}